1. Clone Circular Image View Library from [here](https://github.com/lopspower/CircularImageView) . Then import it to workspace and add as a library in your project.
2. Clone Sticky List Header Library from [here](https://github.com/emilsjolander/StickyListHeaders) and add it as a library in the project.
3. CLone CardSlib Library from [here](https://github.com/gabrielemariotti/cardslib) and add it as a library in the project.
4. CLone FloatingActionButton Library from [here](https://github.com/FaizMalkani/FloatingActionButton.git) and add it as a library in the project.
