package com.cyan.valothaki.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.entity.Answer;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.util.Global;

public class AnswerTextAdapter extends ArrayAdapter<SingleItem> {

	private Context context;
	private int resourceId;
	private List<SingleItem> items;
	private List<Answer> answers;

	// public ChamberTextAdapter(Context context, int resource,
	// List<SingleItem> generalItems) {
	// super(context, resource, generalItems);
	// this.context = context;
	// this.resourceId = resource;
	// this.items = generalItems;
	// }

	public AnswerTextAdapter(Context context, int resource,
			List<SingleItem> clients) {
		super(context, resource, clients);
		this.context = context;
		this.resourceId = resource;
		this.items=clients;
	}

	public AnswerTextAdapter(Context context, int resource,
			List<Answer> clients1,List<SingleItem> clients) {
		super(context, resource, clients);
		this.context = context;
		this.resourceId = resource;
		this.answers=clients1;
		this.items=clients;
	}
	@SuppressLint("NewApi")
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		SingleItem item = items.get(position);
		Answer ansT= answers.get(position); 

		View rowView = convertView;
		if (convertView == null) {
			rowView = inflater.inflate(resourceId, null);
		}

		LinearLayout layout_main = (LinearLayout) rowView
				.findViewById(R.id.layout_main);
		TextView textView_answer = (TextView) rowView
				.findViewById(R.id.tv_answer);
		TextView textView_replier = (TextView) rowView
				.findViewById(R.id.tv_replier);
		TextView textView_datetime = (TextView) rowView
				.findViewById(R.id.tv_datetime);

		Animation animation = AnimationUtils.loadAnimation(getContext(),
				(position > Global.ListLength) ? R.anim.up_from_bottom
						: R.anim.down_from_top);
		rowView.startAnimation(animation);

		Global.ListLength = position;

		int colorIndex = item.id % Constants.COLORS.length;
		layout_main.setBackgroundColor(context.getResources().getColor(
				Constants.COLORS[colorIndex]));
		textView_answer.setText(item.text);
		textView_replier.setText(ansT.getUserName());
		textView_datetime.setText(ansT.getDate()+"\n"+ansT.getTime());

		return rowView;
	}
	// @SuppressLint("NewApi")
	// public View getView(int position, View convertView, ViewGroup parent) {
	//
	// Log.d("ch","vhamber e");
	// LayoutInflater inflater = (LayoutInflater) context
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//
	// Chamber item = chambers.get(position);
	//
	//
	// View rowView = convertView;
	// if (convertView == null) {
	// rowView = inflater.inflate(resourceId, null);
	// }
	//
	// RelativeLayout layout_main = (RelativeLayout)
	// rowView.findViewById(R.id.layout_main);
	// TextView textView_name = (TextView)
	// rowView.findViewById(R.id.tv_chamber_name);
	// TextView textView_number = (TextView)
	// rowView.findViewById(R.id.tv_phone_number);
	// TextView textView_time = (TextView)
	// rowView.findViewById(R.id.tv_doctor_time);
	// TextView textView_address = (TextView)
	// rowView.findViewById(R.id.tv_address);
	// TextView textView_close = (TextView)
	// rowView.findViewById(R.id.tv_closed_day);
	//
	// Animation animation = AnimationUtils.loadAnimation(getContext(),
	// (position > Global.ListLength) ? R.anim.up_from_bottom
	// : R.anim.down_from_top);
	// rowView.startAnimation(animation);
	//
	// Global.ListLength = position;
	//
	// int colorIndex = item.getId() % Constants.COLORS.length;
	// layout_main.setBackgroundColor(context.getResources().getColor(
	// Constants.COLORS[colorIndex]));
	// textView_name.setText(item.getName());
	// textView_number.setText(item.getPhone());
	// textView_time.setText(item.getTime());
	// textView_address.setText(item.getAddress());
	// textView_close.setText(item.getClose());
	//
	// return rowView;
	// }

}