package com.cyan.valothaki.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.util.Global;

public class SingleTextAdapter extends ArrayAdapter<SingleItem> {

	private Context context;
	private int resourceId;
	private List<SingleItem> items;

	public SingleTextAdapter(Context context, int resource,
			List<SingleItem> generalItems) {
		super(context, resource, generalItems);
		this.context = context;
		this.resourceId = resource;
		this.items = generalItems;
	}

	@SuppressLint("NewApi")
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		SingleItem item = items.get(position);

		View rowView = convertView;
		if (convertView == null) {
			rowView = inflater.inflate(resourceId, null);
		}

		TextView textView_name = (TextView) rowView.findViewById(R.id.tv_name);

		ImageView imageview_icon = (ImageView) rowView
				.findViewById(R.id.tv_icon);

		Animation animation = AnimationUtils.loadAnimation(getContext(),
				(position > Global.ListLength) ? R.anim.up_from_bottom
						: R.anim.down_from_top);
		rowView.startAnimation(animation);

		Global.ListLength = position;

		int colorIndex = item.id % Constants.COLORS.length;
		ShapeDrawable drawable = new ShapeDrawable();
		Shape shape = new OvalShape();
		drawable.setShape(shape);
		Paint paint = drawable.getPaint();
		paint.setColor(context.getResources().getColor(
				Constants.COLORS[colorIndex]));

		imageview_icon.setBackground(drawable);
		textView_name.setText(item.text);

		return rowView;
	}

}