package com.cyan.valothaki.adapter;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.user.DashboardActivity;
import com.cyan.valothaki.util.Constants;

/*
 * This adapter class is responsible for populating the expandable list item along 
 * with setting header and child layout. 
 */
@SuppressLint("InflateParams")
public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private List<String> mListDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> mListDataChild;
	private Activity mActivity;

	/*
	 * This is the constructor of this adapter where get the activity context,
	 * list of header items, list of child items and activity.
	 */
	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<String>> listChildData, Activity activity) {
		this.mContext = context;
		this.mListDataHeader = listDataHeader;
		this.mListDataChild = listChildData;
		this.mActivity = activity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChild(int, int) This returns
	 * the child object
	 */
	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildId(int, int) Returns
	 * the child Id.
	 */
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildView(int, int, boolean,
	 * android.view.View, android.view.ViewGroup) This class sets the view for
	 * child and put values to all of its widgets. This also has handler for
	 * call button which calls call intent.
	 */
	@SuppressLint("InflateParams")
	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.card_doctor_expand_details, null);
		}
		LinearLayout llDoc = (LinearLayout) convertView
				.findViewById(R.id.ll_expan);
		int colorIndex = groupPosition + 1 % Constants.COLORS.length;
		llDoc.setBackgroundColor(mContext.getResources().getColor(
				Constants.COLORS1[colorIndex]));
//		TextView tvDept = (TextView) convertView.findViewById(R.id.tv_dept);

		TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
		String []ar=childText.split("#");
		tvName.setText(childText);
//		tvDept.setText(ar[1]);

////		final TextView tvNum = (TextView) convertView
////				.findViewById(R.id.tv_number);
////
////		tvNum.setText(ar[2]);
////		ImageButton mCallBt = (ImageButton) convertView
////				.findViewById(R.id.bt_call);
//
//		mCallBt.setOnClickListener(new OnClickListener() {
//
//			public void onClick(View v) {
//				try {
//					Intent callIntent = new Intent(Intent.ACTION_CALL);
//					callIntent.setData(Uri.parse("tel:" + tvNum.getText()));
//					mContext.startActivity(callIntent);
//				} catch (ActivityNotFoundException activityException) {
//					Log.e("Calling a Phone Number", "Call failed",
//							activityException);
//				}
//			}
//		});
//
//		Button mMoreBt = (Button) convertView.findViewById(R.id.bt_more);
//
//		mMoreBt.setOnClickListener(new OnClickListener() {
//
//			public void onClick(View v) {
//				try {
//					Intent mIntent = new Intent(mContext,
//							DashboardActivity.class);
//					mContext.startActivity(mIntent);
//					mActivity.overridePendingTransition(R.anim.rotate_in,
//							R.anim.rotate_out);
//				} catch (ActivityNotFoundException activityException) {
//					Log.e("Calling a Phone Number", "Call failed",
//							activityException);
//				}
//			}
//		});

		return convertView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildrenCount(int) Returns
	 * number of children.
	 */
	@Override
	public int getChildrenCount(int groupPosition) {
		return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
				.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroup(int) Returns group
	 * object.
	 */
	@Override
	public Object getGroup(int groupPosition) {
		return this.mListDataHeader.get(groupPosition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupCount() Returns number
	 * of groups available.
	 */
	@Override
	public int getGroupCount() {
		return this.mListDataHeader.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupId(int) Returns group
	 * Id for a specific group position.
	 */
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupView(int, boolean,
	 * android.view.View, android.view.ViewGroup) Sets layout of the group
	 * header along with necessary data.
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater
					.inflate(R.layout.card_doctor_name, null);
		}
		LinearLayout llDoc = (LinearLayout) convertView.findViewById(R.id.test);
		int colorIndex = (groupPosition + 1) % Constants.COLORS.length;
		llDoc.setBackgroundColor(mContext.getResources().getColor(
				Constants.COLORS[colorIndex]));
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.tv_name);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#hasStableIds() This methods
	 * returns if the Id of the groups are stable
	 */
	@Override
	public boolean hasStableIds() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
	 * This methods returns if the Id of the children are stable
	 */
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}