package com.cyan.valothaki.adapter;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.util.Constants;

@SuppressLint("NewApi")
public class DoubleTextAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, Filterable {

	private LayoutInflater inflater;

	private Context context;
	private List<SingleItem> items,fixed;
	private boolean isFilterOn;

	// private List<Hospital> hospitals;

	public DoubleTextAdapter(Context context, int resource,
			List<SingleItem> generalItems) {
		super();
		this.context = context;
		this.items = generalItems;
		this.fixed=generalItems;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		inflater = new LayoutInflater(context) {

			@Override
			public LayoutInflater cloneInContext(Context newContext) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		SingleItem item = items.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(
					R.layout.row_double_text_left_circle, parent, false);
			holder.name = (TextView) convertView.findViewById(R.id.tv_name);
			holder.number = (TextView) convertView.findViewById(R.id.tv_number);
			holder.icon = (ImageView) convertView.findViewById(R.id.tv_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		int colorIndex = item.id % Constants.COLORS.length;
		ShapeDrawable drawable = new ShapeDrawable();
		Shape shape = new OvalShape();
		drawable.setShape(shape);
		Paint paint = drawable.getPaint();
		paint.setColor(context.getResources().getColor(
				Constants.COLORS[colorIndex]));

		holder.icon.setBackground(drawable);
		holder.name.setText(item.text);
		holder.number.setTag(item.text);

		return convertView;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = inflater.inflate(R.layout.row_header, parent, false);
			holder.text = (TextView) convertView.findViewById(R.id.tv_head);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}
		// set header text as first char in name
		String headerText = "" + items.get(position).text;
		String head = (String) headerText.subSequence(0, 1);
		holder.text.setText(head);
		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		// return the first character of the country as ID because this is what
		// headers are based upon
		String headerText = "" + items.get(position).text;
		return (headerText.subSequence(0, 1).charAt(0));
	}

	class HeaderViewHolder {
		TextView text;
	}

	class ViewHolder {
		TextView name;
		TextView number;
		ImageView icon;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		Filter filter = new Filter() {
			 
            @Override protected void publishResults(CharSequence constraint, FilterResults results) {
                if (constraint.length() == 0) {
                    isFilterOn = false;
                } else {
                    isFilterOn = true;
                    items = (List<SingleItem>) results.values;
                }

                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<SingleItem> filteredBrands = new ArrayList<SingleItem>();

                if (constraint.length() > 0) {
                    constraint = constraint.toString().toLowerCase();
                    for (SingleItem brand : fixed) {
                        if (brand.text.toLowerCase().startsWith(constraint.toString())) {
                            filteredBrands.add(brand);
                        }
                    }

                    results.count = filteredBrands.size();
                    results.values = filteredBrands;
                }

                return results;
            }
        };

        return filter;
    }
}

