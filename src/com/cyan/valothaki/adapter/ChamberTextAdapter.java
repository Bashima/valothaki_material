package com.cyan.valothaki.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.entity.Chamber;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.util.Global;

public class ChamberTextAdapter extends ArrayAdapter<Chamber> {

	private Context context;
	private int resourceId;
	private List<Chamber> chambers;

	// public ChamberTextAdapter(Context context, int resource,
	// List<SingleItem> generalItems) {
	// super(context, resource, generalItems);
	// this.context = context;
	// this.resourceId = resource;
	// this.items = generalItems;
	// }

	public ChamberTextAdapter(Context context, int resource,
			List<Chamber> generalItems) {
		super(context, resource, generalItems);
		this.context = context;
		this.resourceId = resource;
		this.chambers = generalItems;
	}

	// @SuppressLint("NewApi")
	// public View getView(int position, View convertView, ViewGroup parent) {
	// LayoutInflater inflater = (LayoutInflater) context
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//
	// SingleItem item = items.get(position);
	//
	// View rowView = convertView;
	// if (convertView == null) {
	// rowView = inflater.inflate(resourceId, null);
	// }
	//
	// RelativeLayout layout_main = (RelativeLayout)
	// rowView.findViewById(R.id.layout_main);
	// TextView textView_name = (TextView)
	// rowView.findViewById(R.id.tv_chamber_name);
	// TextView textView_number = (TextView)
	// rowView.findViewById(R.id.tv_phone_number);
	// TextView textView_time = (TextView)
	// rowView.findViewById(R.id.tv_doctor_time);
	// TextView textView_address = (TextView)
	// rowView.findViewById(R.id.tv_address);
	// TextView textView_close = (TextView)
	// rowView.findViewById(R.id.tv_closed_day);
	//
	// Animation animation = AnimationUtils.loadAnimation(getContext(),
	// (position > Global.ListLength) ? R.anim.up_from_bottom
	// : R.anim.down_from_top);
	// rowView.startAnimation(animation);
	//
	// Global.ListLength = position;
	//
	// int colorIndex = item.id % Constants.COLORS.length;
	// layout_main.setBackgroundColor(context.getResources().getColor(
	// Constants.COLORS[colorIndex]));
	// textView_name.setText(item.text);
	// textView_number.setText("4901283409");
	// textView_time.setText("6.00 pm to 10.00 pm");
	// textView_address.setText("safoihwonebfcqweybcqwyefhequwh9");
	// textView_close.setText("closed on thursday");
	//
	// return rowView;
	// }
	@SuppressLint("NewApi")
	public View getView(int position, View convertView, ViewGroup parent) {

		Log.d("ch", "vhamber e");
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Chamber item = chambers.get(position);

		View rowView = convertView;
		if (convertView == null) {
			rowView = inflater.inflate(resourceId, null);
		}

		RelativeLayout layout_main = (RelativeLayout) rowView
				.findViewById(R.id.layout_main);
		TextView textView_name = (TextView) rowView
				.findViewById(R.id.tv_chamber_name);
		TextView textView_number = (TextView) rowView
				.findViewById(R.id.tv_phone_number);
		TextView textView_time = (TextView) rowView
				.findViewById(R.id.tv_doctor_time);
		TextView textView_address = (TextView) rowView
				.findViewById(R.id.tv_address);
		TextView textView_close = (TextView) rowView
				.findViewById(R.id.tv_closed_day);

		Animation animation = AnimationUtils.loadAnimation(getContext(),
				(position > Global.ListLength) ? R.anim.up_from_bottom
						: R.anim.down_from_top);
		rowView.startAnimation(animation);

		Global.ListLength = position;

		int colorIndex = item.getId() % Constants.COLORS.length;
		layout_main.setBackgroundColor(context.getResources().getColor(
				Constants.COLORS[colorIndex]));
		textView_name.setText(item.getName());
		textView_number.setText(item.getPhone());
		textView_time.setText(item.getTime());
		textView_address.setText(item.getAddress());
		textView_close.setText(item.getClose());

		return rowView;
	}

}