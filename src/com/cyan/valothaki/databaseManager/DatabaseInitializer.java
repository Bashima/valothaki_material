package com.cyan.valothaki.databaseManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseInitializer extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private String DB_PATH;
	private static String DB_NAME = "ValoThakiDb";
	private Context context;
	private SQLiteDatabase dataBase = null;

	public DatabaseInitializer(Context context) {
		super(context, DB_NAME, null, DATABASE_VERSION);
		this.context = context;
		DB_PATH = File.separator + "data" + File.separator + "data"
				+ File.separator + context.getPackageName() + "/"
				+ "databases/";
		Log.v("log_tag", "DBPath: " + DB_PATH);

		try {
			createDataBase();
		} catch (Exception e) {
			Log.e("log_tag", "Database Creation Error");
		}

	}

	public void createDataBase() throws IOException {
		boolean dbExist = checkDataBase();
		if (dbExist) {
			Log.v("log_tag", "database does exist");
		} else {
			Log.v("log_tag", "database does not exist");
			this.getReadableDatabase();
			copyDataBase();
		}
	}

	private void copyDataBase() {
		try {
			Log.v("DB", "Copying database");

			Log.d("parameters for db", context.toString());
			InputStream myInput = context.getAssets().open(DB_NAME);
			String outFileName = DB_PATH + DB_NAME;
			OutputStream myOutput = new FileOutputStream(outFileName);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, length);
			}
			Log.d("Database found", "db made");
			myOutput.flush();
			myOutput.close();
			myInput.close();
		} catch (IOException exception) {
			Log.e("DB", "dtabase copy failed", exception);
		}
	}

	private boolean checkDataBase() {

		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public synchronized void close() {
		if (dataBase != null)
			dataBase.close();
		super.close();
	}

}
