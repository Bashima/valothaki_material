
package com.cyan.valothaki.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cyan.valothaki.dao.HospitalDao;
import com.cyan.valothaki.dao.QuestionDao;
import com.cyan.valothaki.entity.Hospital;
import com.cyan.valothaki.entity.Question;

public class  UserSyncQuestion  extends AsyncTask<String, String, String> {

	private Context context;
	int districtId = 0;
	private static final String TAG_SUCCESS = "success";
	JSONParser jsonParser = new JSONParser();
	private static String url_question = "http://192.168.43.115/android/get_questions.php";

	public  UserSyncQuestion (Context context) {
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
	}

	/**
	 * getting All products from url
	 * */
	protected String doInBackground(String... args) {
		// Building Parameters
		List<Question> questions = new ArrayList<Question>();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL
		//params.add(new BasicNameValuePair("did", "'" + districtId + "'"));
		JSONObject json = jsonParser.makeHttpRequest(url_question, "GET",
				params);
		JSONArray hospitalJson = null;

		// Check your log cat for JSON reponse
		Log.d("All Products: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// products found
				// Getting Array of Products
				hospitalJson = json.getJSONArray("products");

				// looping through All Products
				for (int i = 0; i < hospitalJson.length(); i++) {
					JSONObject c = hospitalJson.getJSONObject(i);
					Question quest = new Question();
					quest.setId(c.getInt("questionId"));
					quest.setDescription(c.getString("description"));
					quest.setTime(c.getString("time"));
					quest.setDate(c.getString("date"));
					quest.setUserId(c.getInt("userId"));

					// adding HashList to ArrayList
					questions.add(quest);

				}
			} else {
				// no products found
				// Launch Add New product Activity
				//
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		QuestionDao dao = new QuestionDao(context);
		dao.deleteAndPopulateQuestionTable(questions);
		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog after getting all products
		// pDialog.dismiss();
		// // updating UI from Background Thread
		// runOnUiThread(new Runnable() {
		// public void run() {
		// /**
		// * Updating parsed JSON data into ListView
		// * */
		// ListAdapter adapter = new SimpleAdapter(
		// AllProductsActivity.this, productsList,
		// R.layout.list_item, new String[] { TAG_PID,
		// TAG_NAME},
		// new int[] { R.id.pid, R.id.name });
		// // updating listview
		// setListAdapter(adapter);
		// }
		// });

	}

}