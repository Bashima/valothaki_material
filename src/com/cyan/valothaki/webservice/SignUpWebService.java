package com.cyan.valothaki.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class SignUpWebService extends AsyncTask<String, String, String> {
	JSONParser jsonParser = new JSONParser();
	String firstName, lastName, dateOfBirth, road, city, district, phone,
			email, address, gender, status, password;
	// url to create new product
	private static String url_create_user = "http://192.168.43.115/android/sign_up_user.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";

	public SignUpWebService(String arg) {
		Log.d("arg: ", arg);
		String[] ar = arg.split("\\$");
		Log.d("arg: ", String.valueOf(ar.length));
		firstName = ar[0];
		lastName = ar[1];
		dateOfBirth = ar[2];
		road = ar[3];
		city = ar[4];
		district = ar[5];
		phone = ar[6];
		email = ar[7];
		// address=ar[8];
		gender = ar[8];
		status = ar[9];
		password = ar[10];

	}

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		/*
		 * pDialog = new ProgressDialog(NewProductActivity.this);
		 * pDialog.setMessage("Creating Product..");
		 * pDialog.setIndeterminate(false); pDialog.setCancelable(true);
		 * pDialog.show();
		 */
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {

		Log.d("cponstrict: ", "background");
		// //set($_POST['email']) && isset($_POST['password'])
		// && isset($_POST['dob']) && isset($_POST['gender']) &&
		// isset($_POST['address'])
		// && isset($_POST['phone'])
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("firstName", firstName));
		params.add(new BasicNameValuePair("lastName", lastName));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("dob", dateOfBirth));
		params.add(new BasicNameValuePair("gender", gender));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("road", road));
		params.add(new BasicNameValuePair("district", district));
		params.add(new BasicNameValuePair("phone", phone));
		params.add(new BasicNameValuePair("status", status));

		// getting JSON Object
		// Note that create product url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_create_user, "POST",
				params);

		// check log cat fro response
		Log.d("Create Response", json.toString());

		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// successfully created product
				// / Intent i = new Intent(getApplicationContext(),
				// AllProductsActivity.class);
				// //startActivity(i);

				// closing this screen
				Log.d("success: ", "sucess");
				return "success";
				// finish();
			} else {
				// failed to create product
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog once done
		// pDialog.dismiss();

	}

}