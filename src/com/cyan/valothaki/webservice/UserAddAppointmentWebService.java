package com.cyan.valothaki.webservice;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.cyan.valothaki.constants.ConstantsUser;

import android.os.AsyncTask;
import android.util.Log;

public class UserAddAppointmentWebService extends AsyncTask<String, String, String> {
	JSONParser jsonParser = new JSONParser();
	 String userId,doctorUserId,date,chamberId;
    // url to create new product
    private static String url_create_appointment = "http://192.168.43.115/android/create_appointment.php";
 
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    
    public UserAddAppointmentWebService(String arg)
    {
    	Log.d("arg: ", arg);
    	String []ar=arg.split("\\$");
    	Log.d("arg: ", String.valueOf(ar.length));
    	userId=ConstantsUser.userId;
    	doctorUserId=ar[0];
    	date=ar[1];
    	chamberId=ar[2];
    	
    	
    	
    }
    /**
     * Before starting background thread Show Progress Dialog
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      /*  pDialog = new ProgressDialog(NewProductActivity.this);
        pDialog.setMessage("Creating Product..");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();*/
    }

    /**
     * Creating product
     * */
    protected String doInBackground(String... args) {
       
    	Log.d("cponstrict: ", "background");
////set($_POST['email']) && isset($_POST['password']) 
    	//&& isset($_POST['dob']) && isset($_POST['gender'])  && isset($_POST['address']) 
    	//&& isset($_POST['phone'])
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", userId));
        params.add(new BasicNameValuePair("doctorUserId", doctorUserId));
        params.add(new BasicNameValuePair("date", date));
        params.add(new BasicNameValuePair("chamberId", chamberId));
        params.add(new BasicNameValuePair("status", "pending"));
        

        // getting JSON Object
        // Note that create product url accepts POST method
        JSONObject json = jsonParser.makeHttpRequest(url_create_appointment,
                "POST", params);

        // check log cat fro response
        Log.d("Create Response", json.toString());

        // check for success tag
        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                // successfully created product
              ///  Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                ////startActivity(i);

                // closing this screen
            	Log.d("success: ", "sucess");
            	return "success";
               // finish();
            } else {
                // failed to create product
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(String file_url) {
        // dismiss the dialog once done
        //pDialog.dismiss();
    
    }

}