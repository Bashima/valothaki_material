
package com.cyan.valothaki.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.cyan.valothaki.activity.doctorassistant.AcceptingPatientsActivity;
import com.cyan.valothaki.activity.doctorassistant.DoctorAssistantDashboardActivity;
import com.cyan.valothaki.activity.doctorassistant.ShowPatientListActivity;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.dao.AmbulanceDao;
import com.cyan.valothaki.dao.AppointmentDao;
import com.cyan.valothaki.dao.UserDao;
import com.cyan.valothaki.entity.Ambulance;
import com.cyan.valothaki.entity.Appointment;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.PatientListForAcceptanceModel;

public class DoctorPendingRequest  extends AsyncTask<String, String, String> {

	private Context context;
	int choice = 0;
	private static final String TAG_SUCCESS = "success";
	JSONParser jsonParser = new JSONParser();
	private static String url_pending = "http://192.168.43.115/android/get_pending_appointment.php";
	private static String url_accepted = "http://192.168.43.115/android/get_accepted_appointment.php";

	public DoctorPendingRequest(Context context, int dist) {
		this.choice = dist;
		this.context = context;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// pDialog = new ProgressDialog(AllProductsActivity.this);
		// pDialog.setMessage("Loading products. Please wait...");
		// pDialog.setIndeterminate(false);
		// pDialog.setCancelable(false);
		// pDialog.show();
	}

	/**
	 * getting All products from url
	 * */
	protected String doInBackground(String... args) {
		// Building Parameters
		JSONObject json = null;
		UserDao uDao=new UserDao(context);
		ArrayList<Appointment> appointments = new ArrayList<Appointment>();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL
		params.add(new BasicNameValuePair("did", "'" + ConstantsUser.docId + "'"));
	//	if()
		if(choice==1)
			json = jsonParser.makeHttpRequest(url_pending, "GET",params);
		else
		{
			json = jsonParser.makeHttpRequest(url_accepted, "GET",params);
		}
			
		JSONArray appointmentJson = null;

		// Check your log cat for JSON reponse
		Log.d("All Products: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// products found
				// Getting Array of Products
				appointmentJson = json.getJSONArray("products");

				// looping through All Products
				for (int i = 0; i < appointmentJson.length(); i++) {
					JSONObject c = appointmentJson.getJSONObject(i);
					Appointment appointment = new Appointment();
					appointment.setAppointmentId(c.getInt("appointmentId"));
					appointment.setUserId(c.getInt("userId"));
					appointment.setDate(c.getString("date"));
					appointment.setTime(c.getString("time"));
					appointment.setUserName(c.getString("userName"));
					appointment.setChamberId(c.getInt("chamberId"));
					
					Log.d("adding:", "apointment");
					// adding HashList to ArrayList
					appointments.add(appointment);

				}
			} else {
				// no products found
				// Launch Add New product Activity
				//
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		AppointmentDao dao=new AppointmentDao(context, choice);
		dao.deleteAndPopulateAmbulanceTable(appointments);
		

		if(choice==1)
		{
			Intent intent = new Intent();
			intent.setClass(context, AcceptingPatientsActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
		else
		{
			Intent intent = new Intent();
			intent.setClass(context, ShowPatientListActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			
		}
		
	
		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog after getting all products
		// pDialog.dismiss();
		// // updating UI from Background Thread
		// runOnUiThread(new Runnable() {
		// public void run() {
		// /**
		// * Updating parsed JSON data into ListView
		// * */
		// ListAdapter adapter = new SimpleAdapter(
		// AllProductsActivity.this, productsList,
		// R.layout.list_item, new String[] { TAG_PID,
		// TAG_NAME},
		// new int[] { R.id.pid, R.id.name });
		// // updating listview
		// setListAdapter(adapter);
		// }
		// });

	}

}