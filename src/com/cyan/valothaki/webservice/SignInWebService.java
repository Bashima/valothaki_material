package com.cyan.valothaki.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.cyan.valothaki.activity.doctorassistant.DoctorAssistantDashboardActivity;
import com.cyan.valothaki.activity.user.DashboardActivity;
import com.cyan.valothaki.constants.ConstantsUser;

public class SignInWebService extends AsyncTask<String, String, String> {

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	private Context context;
	JSONParser jsonParser = new JSONParser();
	private static String url_signIn = "http://192.168.43.115//android/sign_in.php";
	private static final String TAG_SUCCESS = "success";
	String email, password;

	public SignInWebService(Context context, String email, String password) {
		this.context = context;
		this.email = email;
		this.password = password;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

	}

	/**
	 * Getting product details in background thread
	 * */
	protected String doInBackground(String... args) {

		// updating UI from Background Thread

		// Check for success tag
		int success;
		String type;
		try {
			// Building Parameters
			Log.d("email", email);
			Log.d("password", password);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("email", "'" + email + "'"));
			params.add(new BasicNameValuePair("password", "'" + password + "'"));

			// getting product details by making HTTP request
			// Note that product details url will use GET request
			JSONObject json = jsonParser.makeHttpRequest(url_signIn, "GET",
					params);

			// check your log for json response
			Log.d("Single Product Details", json.toString());

			// json success tag
			success = json.getInt(TAG_SUCCESS);
			if (success == 1) {
				// successfully received product details
				JSONArray productObj = json.getJSONArray("product"); // JSON
																		// Array

				// get first product object from JSON Array
				JSONObject product = productObj.getJSONObject(0);

				// product with this pid found
				// Edit Text
				ConstantsUser.response = product.getString("status");
				ConstantsUser.userId = product.getString("userId");
				type=product.getString("type");
				Log.d("resp: ", ConstantsUser.response + " "
						+ ConstantsUser.userId);
				if (ConstantsUser.response.equals("active") && type.equals("1") ) {
					Intent intent = new Intent();
					intent.setClass(context, DashboardActivity.class);
					context.startActivity(intent);

				}
				else if(ConstantsUser.response.equals("active") && type.equals("2"))
				{
					ConstantsUser.docId=product.getString("docId");
					Log.d("doc id isL ", ConstantsUser.docId);
					Intent intent = new Intent();
					intent.setClass(context, DoctorAssistantDashboardActivity.class);
					context.startActivity(intent);

					
				}

			} else {
				// product with pid not found
				Log.d("resp: ", "product with pid not found");

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog once got all details

	}
}