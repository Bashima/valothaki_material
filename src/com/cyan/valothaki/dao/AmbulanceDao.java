package com.cyan.valothaki.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Ambulance;

public class AmbulanceDao {
	private DatabaseInitializer initializer;
	int district;

	public AmbulanceDao(Context context, int dis) {
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		this.district = dis;

	}

	public ArrayList<String> PopulateAmbulance() {
		ArrayList<String> ambulances = new ArrayList<String>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(district) };
		Log.d("args: ", String.valueOf(district));
		String query = "Select * from Ambulance where districtid = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {

				String tempS = cursor.getString(1);
				Log.d("list", tempS);
				ambulances.add(tempS);

			} while (cursor.moveToNext());
		}
		return ambulances;
	}

	public Ambulance getAmbulance(int id) {
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from Ambulance where AmbulanceId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				Ambulance amb = new Ambulance();

				String tempS = cursor.getString(1);
				Log.d("list", tempS);
				amb.setId(cursor.getInt(0));
				amb.setName(cursor.getString(1));
				amb.setAreaId(cursor.getInt(2));
				amb.setDistrictId(cursor.getInt(4));
				amb.setFee(cursor.getString(5));
				amb.setPhone(cursor.getString(6));
				// ambulances.add(tempS);
				return amb;

			} while (cursor.moveToNext());
		}
		return null;
	}

	public void deleteAndPopulateAmbulanceTable(List<Ambulance> ambulances) {
		SQLiteDatabase database = initializer.getReadableDatabase();
		database.delete("Ambulance", null, null);
		ContentValues values = new ContentValues();
		for (int i = 0; i < ambulances.size(); i++) {
			Ambulance hosp = ambulances.get(i);
			values.put("Name", hosp.getName());
			values.put("AmbulanceId", hosp.getId());
			values.put("DistrictId", district);
			values.put("AreaId", hosp.getAreaId());

			values.put("Phone", hosp.getPhone());

			values.put("Fee", hosp.getFee());

			Log.d("hospital: ", hosp.getName());
			database.insert("Ambulance", null, values);

		}
		database.close();

	}

}
