package com.cyan.valothaki.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.BloodBank;

public class BloodBankDao {
	private DatabaseInitializer initializer;
	int district;

	public BloodBankDao(Context context, int dis) {
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		this.district = dis;

	}

	public ArrayList<String> PopulateBloodBank() {
		ArrayList<String> bloodBanks = new ArrayList<String>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(district) };
		Log.d("args: ", String.valueOf(district));
		String query = "Select * from BloodBank where districtid = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				/*
				 * Hospital tempHosp=new Hospital();
				 * tempHosp.setId(cursor.getInt(0)); //String
				 * name=cursor.getString(1); //String email=cursor.getString(1);
				 * tempHosp.setDistrictId(district);
				 * tempHosp.setName(cursor.getString(2));
				 * tempHosp.setEmail(cursor.getString(3));
				 * tempHosp.setAddress(cursor.getString(4));
				 * tempHosp.setWebsite(cursor.getString(5));
				 * 
				 * Log.d("list", tempHosp.getName()); hospitals.add(tempHosp);
				 */
				String tempS = cursor.getString(1);
				Log.d("list", tempS);
				bloodBanks.add(tempS);

			} while (cursor.moveToNext());
		}
		return bloodBanks;
	}

	public BloodBank getBloodBank(int id) {
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from BloodBank where bloodBankId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				BloodBank blood = new BloodBank();

				String tempS = cursor.getString(1);
				Log.d("list", tempS);
				blood.setId(cursor.getInt(0));
				blood.setName(cursor.getString(1));
				blood.setAreaId(cursor.getInt(5));
				blood.setDistrictId(cursor.getInt(6));
				blood.setPhone(cursor.getString(2));
				blood.setWebsite(cursor.getString(4));
				blood.setEmail(cursor.getString(3));
				blood.setAddress(cursor.getString(7));
				// ambulances.add(tempS);
				return blood;

			} while (cursor.moveToNext());
		}
		return null;
	}

	public void deleteAndPopulateBloodBankTable(List<BloodBank> bloods) {
		SQLiteDatabase database = initializer.getReadableDatabase();
		database.delete("BloodBank", null, null);
		ContentValues values = new ContentValues();
		for (int i = 0; i < bloods.size(); i++) {
			BloodBank hosp = bloods.get(i);
			values.put("Name", hosp.getName());
			values.put("BloodBankId", hosp.getId());
			values.put("DistrictId", district);
			values.put("AreaId", hosp.getAreaId());
			values.put("Email", hosp.getEmail());
			values.put("Phone", hosp.getPhone());
			values.put("WebSite", hosp.getWebsite());
			values.put("Address", hosp.getAddress());

			Log.d("hospital: ", hosp.getName());
			database.insert("BloodBank", null, values);

		}
		database.close();

	}

}
