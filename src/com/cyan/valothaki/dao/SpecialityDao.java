package com.cyan.valothaki.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Specialization;

public class SpecialityDao {
	private DatabaseInitializer initializer;
	int district;
	public SpecialityDao(Context context)
	{
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		
	}
	
	public ArrayList<String> Populate()
	{
		ArrayList<String> districts=new ArrayList<String>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		Cursor cursor = db.query("SPECIALIZATION", null, null, null, null,null, null);
		if(cursor.moveToFirst())
		{
			do
			{
//				int temp=cursor.getInt(0);
				String tempS=cursor.getString(1);
				Log.d("list", tempS);
				districts.add(tempS);
				
			}while(cursor.moveToNext());
		}
		db.close();
		return districts;
	}
	public ArrayList<Specialization> PopulateSpec()
	{
		ArrayList<Specialization> districts=new ArrayList<Specialization>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		Cursor cursor = db.query("SPECIALIZATION", null, null, null, null,null, null);
		if(cursor.moveToFirst())
		{
			do
			{
				Specialization temp=new Specialization();
//				int temp=cursor.getInt(0);
				temp.setSpecializationId(cursor.getInt(0));
				temp.setName(cursor.getString(1));
				//Log.d("list", tempS);
				districts.add(temp);
				
			}while(cursor.moveToNext());
		}
		db.close();
		return districts;
	}
	public String speciality(int id)
	{
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from Specialization where specializationId = ?";
		Cursor cursor = db.rawQuery(query, args);
		if(cursor.moveToFirst())
		{
			do
			{
				return cursor.getString(1);
			
			}while(cursor.moveToNext());
		}
		db.close();
		return null;
	}
	
	
	
	

}
