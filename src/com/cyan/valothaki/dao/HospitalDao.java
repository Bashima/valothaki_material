package com.cyan.valothaki.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Hospital;

public class HospitalDao {
	private DatabaseInitializer initializer;
	int district;
	public HospitalDao(Context context,int dis)
	{
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		this.district=dis;
		
	}
	
	public ArrayList<Hospital> PopulateHospital()
	{
		ArrayList<Hospital> hospitals=new ArrayList<Hospital>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args={String.valueOf(district)};
		String query="Select * from Hospital where districtid = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if(cursor.moveToFirst())
		{
			do
			{
				Hospital tempHosp=new Hospital();
				tempHosp.setId(cursor.getInt(0));
				//String name=cursor.getString(1);
				//String email=cursor.getString(1);
				tempHosp.setDistrictId(district);
				tempHosp.setName(cursor.getString(2));
				tempHosp.setEmail(cursor.getString(3));
				tempHosp.setAddress(cursor.getString(4));
				tempHosp.setWebsite(cursor.getString(5));
				
				Log.d("list", tempHosp.getName());
				hospitals.add(tempHosp);
				
			}while(cursor.moveToNext());
		}
		return hospitals;
	}
	
	public void deleteAndPopulateHospitalTable(List<Hospital> hospitals)
	{
		 SQLiteDatabase database = initializer.getReadableDatabase();
		 database.delete("Hospital", null, null);
		 ContentValues values = new ContentValues();
		 for(int i=0;i<hospitals.size();i++)
		 {
			 Hospital hosp=hospitals.get(i);
			 values.put("Name", hosp.getName() );
			 values.put("HospitalId", hosp.getId());
			 values.put("DistrictId", district);
			 values.put("AreaId", hosp.getAreaId());
			 values.put("Email", hosp.getEmail());
			 values.put("Phone", hosp.getPhone());
			 values.put("WebSite", hosp.getWebsite());
			 
			 Log.d("hospital: ",hosp.getName());
			 database.insert("Hospital", null, values);

			 
		 }
		 database.close();

		
	}
	
	

}
