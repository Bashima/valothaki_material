package com.cyan.valothaki.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Doctor;
import com.cyan.valothaki.entity.Hospital;

public class HospitalDetailDao {
	private DatabaseInitializer initializer;
	int hospitalId;

	public HospitalDetailDao(Context context, int dis) {
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		this.hospitalId = dis;

	}

	public Hospital HospitalDetail() {
		Hospital tempHosp = null;
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(hospitalId) };
		String query = "Select * from hospital where HospitalId = ?";
		String query1="Select * from hospitalrating where HospitalId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Cursor cursor1 = db.rawQuery(query1, args);

		if (cursor.moveToFirst()) {
			double rating=0;
			do {
				tempHosp = new Hospital();
				tempHosp.setId(cursor.getInt(0));
				// String name=cursor.getString(1);
				// String email=cursor.getString(1);
				tempHosp.setDistrictId(hospitalId);
				tempHosp.setName(cursor.getString(2));
				tempHosp.setEmail(cursor.getString(3));
				tempHosp.setAddress(cursor.getString(4));
				tempHosp.setWebsite(cursor.getString(5));
				tempHosp.setPhone(cursor.getString(7));
				
				

//				 String[] args1={String.valueOf(hospitalId)};
				query = "Select * from doctor where HospitalId  = ?";
				Cursor cursorDoc = db.rawQuery(query, args);
				
				ArrayList<Doctor> docList = new ArrayList<Doctor>();
				if(cursor1.moveToFirst())
				{
					do
					{
						String rate1=cursor1.getString(3);
						rating+=Double.parseDouble(rate1);
						
						
					}while(cursor.moveToNext());
				}
				tempHosp.setRate(String.valueOf(rating));
				if (cursorDoc.moveToFirst()) {
					do {
						Doctor tempDoc = new Doctor();
						tempDoc.setUserId(cursorDoc.getInt(0));
						tempDoc.setDoctorId(cursorDoc.getString(1));
						tempDoc.setQualification(cursorDoc.getString(2));
						tempDoc.setSpecializationId(cursorDoc.getInt(3));
						tempDoc.setHospitalId(cursorDoc.getInt(4));
						
						//fetching name from user
						String[] args1={String.valueOf(tempDoc.getUserId())};
						query = "Select * from user where userId  = ?";
						Cursor cursorUser = db.rawQuery(query, args1);
						if(cursorUser.moveToFirst())
						{
							do
							{
								tempDoc.setName(cursorUser.getString(1));
								tempDoc.setEmail(cursorUser.getString(2));
								tempDoc.setPhone(cursorUser.getString(6));
								
								break;
								
							}
							while(cursorUser.moveToNext());
						}
						
						String[] args2={String.valueOf(tempDoc.getSpecializationId())};
						query = "Select * from specialization where specializationId  = ?";
						Cursor cursorSpec = db.rawQuery(query, args2);
						if(cursorSpec.moveToFirst())
						{
							do
							{
								tempDoc.setSpecialization(cursorSpec.getString(1));
								break;
								
							}while(cursorSpec.moveToNext());
						}
						
						
						
						
						
						docList.add(tempDoc);

					} while (cursorDoc.moveToNext());
				}
				tempHosp.setDocs(docList);
				Log.d("list", tempHosp.getName());
				break;

			} while (cursor.moveToNext());
		}
		return tempHosp;

	}

}
