package com.cyan.valothaki.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Answer;
import com.cyan.valothaki.entity.Chamber;
import com.cyan.valothaki.entity.Hospital;
import com.cyan.valothaki.entity.Question;

public class QuestionDao {
	private DatabaseInitializer initializer;
	public QuestionDao(Context context)
	{
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		
	}
	
	public ArrayList<String> DemoPopulate()
	{
		ArrayList<String> questions=new ArrayList<String>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		Cursor cursor = db.query("Question", null, null, null, null,null, null);
		if(cursor.moveToFirst())
		{
			do
			{
//				int temp=cursor.getInt(0);
				String tempS=cursor.getString(1);
				Log.d("list", tempS);
				questions.add(tempS);
				
			}while(cursor.moveToNext());
		}
		db.close();
		return questions;
	}
	public void deleteAndPopulateQuestionTable(List<Question> questions)
	{
		 SQLiteDatabase database = initializer.getReadableDatabase();
		 database.delete("Question", null, null);
		 ContentValues values = new ContentValues();
		 for(int i=0;i<questions.size();i++)
		 {
			 Question ques=questions.get(i);
			 values.put("Description", ques.getDescription() );
			 values.put("QuestionId", ques.getId());
			 values.put("UserId", ques.getUserId());
			 values.put("Date", ques.getDate());
			 values.put("Time", ques.getTime());
			 
			 Log.d("ques: ",ques.getDescription());
			 database.insert("Question", null, values);

			 
		 }
		 database.close();

		
	}
	public ArrayList<Answer> getAnswer(int questionId) {
		ArrayList<Answer> chambers = new ArrayList<Answer>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(questionId) };
		Log.d("args: ", String.valueOf(questionId));
		String query = "Select * from AnswerTable where QuestionId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				Answer temp = new Answer();
				temp.setAnswerId(cursor.getInt(0));
				temp.setUserId(cursor.getInt(5));
				temp.setQuestionId(cursor.getInt(4));
				temp.setDescription(cursor.getString(1));
				temp.setDate(cursor.getString(2));
				temp.setTime(cursor.getString(3));

				String[] args1 = { String.valueOf(temp.getUserId()) };
				String query1 = "Select * from User where UserId = ?";
				Cursor cursor1 = db.rawQuery(query1, args1);
				if(cursor1.moveToFirst())
				{
					do
					{
						temp.setUserName(cursor1.getString(1));
						break;
						
					}while(cursor1.moveToNext());
				}
				
				
				
				chambers.add(temp);

			} while (cursor.moveToNext());

		}
		db.close();
		return chambers;
	}
	
	public Question getQuestion(int id)
	{
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from Question where QuestionId = ?";
		Cursor cursorQues = db.rawQuery(query, args);
		Question ret=new Question();

		if (cursorQues.moveToFirst()) {
			do
			{
				ret.setId(cursorQues.getInt(0));
				ret.setDescription(cursorQues.getString(1));
				ret.setDate(cursorQues.getString(2));
				ret.setTime(cursorQues.getString(3));
				ret.setUserId(cursorQues.getInt(4));
				
				String[] args1 = { String.valueOf(ret.getUserId()) };
				String query1 = "Select * from User where UserId = ?";
				Cursor cursor = db.rawQuery(query1, args1);
				if(cursor.moveToFirst())
				{
					do
					{
						ret.setUserName(cursor.getString(1));
						break;
						
					}while(cursor.moveToNext());
				}
				
				
				return ret;
				
				
			}while(cursorQues.moveToNext());
		
		}
		
		return ret;
		
	}
	
	public void deleteAndPopulateAnswerTable(List<Answer> answers,int id)
	{
		 SQLiteDatabase database = initializer.getReadableDatabase();
		 Log.d("qid: ", String.valueOf(id));
		 database.delete("AnswerTable", "QuestionId = "+ id, null);
		 ContentValues values = new ContentValues();
		 for(int i=0;i<answers.size();i++)
		 {
			 Answer ans=answers.get(i);
			 values.put("Description", ans.getDescription() );
			 values.put("QuestionId", ans.getQuestionId());
			 values.put("UserId", ans.getUserId());
			 values.put("Date", ans.getDate());
			 values.put("Time", ans.getTime());
			 values.put("AnswerId", ans.getAnswerId());
			 
			 Log.d("ques: ",ans.getDescription());
			 database.insert("AnswerTable", null, values);

			 
		 }
		 database.close();

		
	}

}
