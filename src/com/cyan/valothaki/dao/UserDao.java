package com.cyan.valothaki.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;

public class UserDao {

	private DatabaseInitializer initializer;
	int district;
	public UserDao(Context context)
	{
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		
	}
	public String getName(int id)
	{
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from User where userId = ?";
		Cursor cursor = db.rawQuery(query, args);
		if(cursor.moveToFirst())
		{
			do
			{
				return cursor.getString(1);
			
			}while(cursor.moveToNext());
		}
		db.close();
		return null;
	}
}
