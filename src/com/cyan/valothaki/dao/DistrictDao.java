package com.cyan.valothaki.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;

public class DistrictDao {
	private DatabaseInitializer initializer;
	public DistrictDao(Context context)
	{
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		
	}
	
	public ArrayList<String> DemoPopulate()
	{
		ArrayList<String> districts=new ArrayList<String>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		Cursor cursor = db.query("District", null, null, null, null,null, null);
		if(cursor.moveToFirst())
		{
			do
			{
//				int temp=cursor.getInt(0);
				String tempS=cursor.getString(1);
				Log.d("list", tempS);
				districts.add(tempS);
				
			}while(cursor.moveToNext());
		}
		return districts;
	}
	
	

}
