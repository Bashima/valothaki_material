package com.cyan.valothaki.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Ambulance;
import com.cyan.valothaki.entity.Appointment;

public class AppointmentDao {
	private DatabaseInitializer initializer;
	int docId;
	public AppointmentDao(Context context, int dis) {
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();
		this.docId = dis;

	}
	public void deleteAndPopulateAmbulanceTable(List<Appointment> appointments) {
		SQLiteDatabase database = initializer.getReadableDatabase();
		database.delete("appointment", "DoctorUserId = "+ConstantsUser.docId, null);
		ContentValues values = new ContentValues();
		for (int i = 0; i < appointments.size(); i++) {
			Appointment appointment = appointments.get(i);
			values.put("AppointmentId", appointment.getAppointmentId());
			values.put("DoctorUserId", ConstantsUser.docId);
			values.put("UserId", appointment.getUserId());
			values.put("Date", appointment.getDate());

			values.put("Time", appointment.getTime());
			values.put("UserName", appointment.getUserName());
			


			Log.d("hospital: ", String.valueOf(appointment.getUserId()));
			database.insert("appointment", null, values);

		}
		database.close();

	}
	public ArrayList<Appointment> PopulateAppointment() {
		ArrayList<Appointment> appointments = new ArrayList<Appointment>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(ConstantsUser.docId) };
		Log.d("args: ", String.valueOf(ConstantsUser.docId));
		String query = "Select * from Appointment where DoctorUserId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				Appointment temp=new Appointment();
				temp.setAppointmentId(cursor.getInt(0));
				temp.setUserId(cursor.getInt(2));
				temp.setDoctorUserId(cursor.getInt(1));
				temp.setUserName(cursor.getString(8));
				temp.setDate(cursor.getString(3));
				temp.setTime(cursor.getString(4));
				temp.setChamberId(cursor.getInt(6));
				
				Log.d("added", "added");
				appointments.add(temp);

			} while (cursor.moveToNext());
		}
		return appointments;
	}


}
