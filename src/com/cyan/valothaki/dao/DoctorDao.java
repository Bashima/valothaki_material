package com.cyan.valothaki.dao;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cyan.valothaki.databaseManager.DatabaseInitializer;
import com.cyan.valothaki.entity.Chamber;
import com.cyan.valothaki.entity.Doctor;

public class DoctorDao {

	private DatabaseInitializer initializer;

	public DoctorDao(Context context) {
		initializer = new DatabaseInitializer(context);
		initializer.getWritableDatabase().close();

	}

	public Doctor getDoctor(int id) {
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(id) };
		String query = "Select * from Doctor where DoctorUserId = ?";
		Cursor cursorDoc = db.rawQuery(query, args);

		if (cursorDoc.moveToFirst()) {
			do {
				Doctor tempDoc = new Doctor();
				tempDoc.setUserId(cursorDoc.getInt(0));
				tempDoc.setDoctorId(cursorDoc.getString(1));
				tempDoc.setQualification(cursorDoc.getString(2));
				tempDoc.setSpecializationId(cursorDoc.getInt(3));
				tempDoc.setHospitalId(cursorDoc.getInt(4));

				// fetching name from user
				String[] args1 = { String.valueOf(tempDoc.getUserId()) };
				query = "Select * from user where userId  = ?";
				Cursor cursorUser = db.rawQuery(query, args1);
				if (cursorUser.moveToFirst()) {
					do {
						tempDoc.setName(cursorUser.getString(1));
						tempDoc.setEmail(cursorUser.getString(2));
						tempDoc.setPhone(cursorUser.getString(6));

						break;

					} while (cursorUser.moveToNext());
				}

				String[] args2 = { String
						.valueOf(tempDoc.getSpecializationId()) };
				query = "Select * from specialization where specializationId  = ?";
				Cursor cursorSpec = db.rawQuery(query, args2);
				if (cursorSpec.moveToFirst()) {
					do {
						tempDoc.setSpecialization(cursorSpec.getString(1));
						break;

					} while (cursorSpec.moveToNext());
				}

				return tempDoc;

			} while (cursorDoc.moveToNext());
		}
		return null;

	}

	public ArrayList<Chamber> getChamber(int docId) {
		ArrayList<Chamber> chambers = new ArrayList<Chamber>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(docId) };
		Log.d("args: ", String.valueOf(docId));
		String query = "Select * from Chamber where DoctorUserId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				Chamber temp = new Chamber();
				temp.setId(cursor.getInt(0));
				temp.setName(cursor.getString(5));
				temp.setPhone(cursor.getString(3));
				temp.setTime(cursor.getString(6));
				temp.setClose(cursor.getString(7));
				temp.setAddress(cursor.getString(2));

				chambers.add(temp);

			} while (cursor.moveToNext());

		}
		return chambers;
	}

	public ArrayList<Doctor> getDocList(int district, int specialId) {
		ArrayList<Doctor> list = new ArrayList<Doctor>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String[] args = { String.valueOf(specialId) };
		Log.d("args: ", String.valueOf(specialId));
		String query = "Select * from Doctor where SpecializationId = ?";
		Cursor cursor = db.rawQuery(query, args);
		Log.d("query", query);
		if (cursor.moveToFirst()) {
			do {
				//
				Doctor tempDoc = new Doctor();
				tempDoc.setUserId(cursor.getInt(0));
				tempDoc.setHospitalId(cursor.getInt(4));
				// fetching name from user
				String[] args1 = { String.valueOf(tempDoc.getUserId()) };
				query = "Select * from user where userId  = ?";
				Cursor cursorUser = db.rawQuery(query, args1);

				if (cursorUser.moveToFirst()) {

					do {
						tempDoc.setName(cursorUser.getString(1));
						// tempDoc.setEmail(cursorUser.getString(2));
						// tempDoc.setPhone(cursorUser.getString(6));

						break;

					} while (cursorUser.moveToNext());
				}
				String[] args2 = { String.valueOf(tempDoc.getHospitalId()) };
				query = "Select * from Hospital where hospitalId  = ?";
				Cursor cursorUser1 = db.rawQuery(query, args2);

				if (cursorUser1.moveToFirst()) {

					do {
						tempDoc.setHospital(cursorUser1.getString(1));
						// tempDoc.setEmail(cursorUser.getString(2));
						// tempDoc.setPhone(cursorUser.getString(6));

						break;

					} while (cursorUser1.moveToNext());
				}

				//@SuppressWarnings("unused")
				String[] args3 = { String.valueOf(specialId) };
				//@SuppressWarnings("unused")
				String query2 = "Select * from Specialization where specializationId = ?";
				Cursor cursor2 = db.rawQuery(query2, args3);
				if (cursor2.moveToFirst()) {
					do {
						tempDoc.setSpecialization(cursor2.getString(1));
						break;

					} while (cursor2.moveToNext());
				}

				list.add(tempDoc);

			} while (cursor.moveToNext());
		}
		db.close();
		return list;

	}

	public ArrayList<Doctor> PopulateSpec(int specialId) {
		ArrayList<Doctor> doctors = new ArrayList<Doctor>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String query = "Select * from Doctor where SpecializationId = ?";
		String[] args = { String.valueOf(specialId) };
		Cursor cursor = db.rawQuery(query, args);
		if (cursor.moveToFirst()) {
			do {
				//
				Doctor tempDoc = new Doctor();
				tempDoc.setUserId(cursor.getInt(0));
				tempDoc.setDoctorId(cursor.getString(1));
				tempDoc.setQualification(cursor.getString(2));
				tempDoc.setSpecializationId(cursor.getInt(3));
				tempDoc.setHospitalId(cursor.getInt(4));

				// fetching name from user
				String[] args1 = { String.valueOf(tempDoc.getUserId()) };
				query = "Select * from user where userId  = ?";
				Cursor cursorUser = db.rawQuery(query, args1);
				if (cursorUser.moveToFirst()) {
					do {
						tempDoc.setName(cursorUser.getString(1));
						tempDoc.setEmail(cursorUser.getString(2));
						tempDoc.setPhone(cursorUser.getString(6));

						break;

					} while (cursorUser.moveToNext());
				}

				doctors.add(tempDoc);

			} while (cursor.moveToNext());
		}
		return doctors;
	}
	
	public ArrayList<Doctor> PopulateSpec(int specialId,int hospitalId,int flag) {
		ArrayList<Doctor> doctors = new ArrayList<Doctor>();
		SQLiteDatabase db = initializer.getReadableDatabase();
		String query = "Select * from Doctor where SpecializationId = ? and HospitalId= ?";
		String[] args = { String.valueOf(specialId),String.valueOf(hospitalId) };
		Cursor cursor = db.rawQuery(query, args);
		if (cursor.moveToFirst()) {
			do {
				//
				Doctor tempDoc = new Doctor();
				tempDoc.setUserId(cursor.getInt(0));
				tempDoc.setDoctorId(cursor.getString(1));
				tempDoc.setQualification(cursor.getString(2));
				tempDoc.setSpecializationId(cursor.getInt(3));
				tempDoc.setHospitalId(cursor.getInt(4));

				// fetching name from user
				String[] args1 = { String.valueOf(tempDoc.getUserId()) };
				query = "Select * from user where userId  = ?";
				Cursor cursorUser = db.rawQuery(query, args1);
				if (cursorUser.moveToFirst()) {
					do {
						tempDoc.setName(cursorUser.getString(1));
						tempDoc.setEmail(cursorUser.getString(2));
						tempDoc.setPhone(cursorUser.getString(6));

						break;

					} while (cursorUser.moveToNext());
				}

				doctors.add(tempDoc);

			} while (cursor.moveToNext());
		}
		return doctors;
	}


}
