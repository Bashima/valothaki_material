package com.cyan.valothaki.entity;

public class Doctor {
	private int userId,specializationId,hospitalId;
	private String name,email,doctorId,qualification,phone,specialization,hospital;
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	public Doctor()
	{
		
	}
	public Doctor(int userId, int specializationId, int hospitalId,
			String name, String email, String doctorId, String qualification,
			String phone) {
		//super();
		this.userId = userId;
		this.specializationId = specializationId;
		this.hospitalId = hospitalId;
		this.name = name;
		this.email = email;
		this.doctorId = doctorId;
		this.qualification = qualification;
		this.phone = phone;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Doctor(int userId, int specializationId, String name, String email,
			String doctorId, String qualification) {
		//super();
		this.userId = userId;
		this.specializationId = specializationId;
		this.name = name;
		this.email = email;
		this.doctorId = doctorId;
		this.qualification = qualification;
	}
	public Doctor(int userId, int specializationId, int hospitalId,
			String name, String email, String doctorId, String qualification) {
		//super();
		this.userId = userId;
		this.specializationId = specializationId;
		this.hospitalId = hospitalId;
		this.name = name;
		this.email = email;
		this.doctorId = doctorId;
		this.qualification = qualification;
	}
	public int getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(int hospitalId) {
		this.hospitalId = hospitalId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getSpecializationId() {
		return specializationId;
	}
	public void setSpecializationId(int specializationId) {
		this.specializationId = specializationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
	

}
