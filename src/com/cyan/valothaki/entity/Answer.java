package com.cyan.valothaki.entity;

public class Answer {
	private int answerId,questionId,userId;
	private String date,time,description,userName;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Answer(){}
	public Answer(int answerId, int questionId, int userId, String date,
			String time, String description) {
		super();
		this.answerId = answerId;
		this.questionId = questionId;
		this.userId = userId;
		this.date = date;
		this.time = time;
		this.description = description;
	}
	public int getAnswerId() {
		return answerId;
	}
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
