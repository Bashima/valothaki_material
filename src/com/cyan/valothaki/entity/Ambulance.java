package com.cyan.valothaki.entity;

public class Ambulance {
	public Ambulance(int id, int districtId, int areaId, String name, String fee) {
		super();
		this.id = id;
		this.districtId = districtId;
		this.areaId = areaId;
		this.name = name;
		this.fee = fee;
	}
	public Ambulance(){}
	private int id,districtId,areaId;
	private String name,fee,phone;
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	

}
