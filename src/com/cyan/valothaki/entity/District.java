package com.cyan.valothaki.entity;

public class District {
	private int id;
	private String name;
	public District()
	{
		
	}
	public District(int i,String n)
	{
		this.id=i;
		this.name=n;
	}

	public int getId()
	{
		return this.id;
	}
	
	public String getName()
	{
		return this.name;
	}
}
