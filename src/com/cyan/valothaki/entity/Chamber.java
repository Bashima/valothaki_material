package com.cyan.valothaki.entity;

public class Chamber {
	private int id;
	private String name,time,phone,address,close;
	public Chamber(){}
	public Chamber(int id, String name, String time, String phone,
			String address, String close) {
		super();
		this.id = id;
		this.name = name;
		this.time = time;
		this.phone = phone;
		this.address = address;
		this.close = close;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}

}
