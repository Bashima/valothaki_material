package com.cyan.valothaki.entity;

public class BloodBank {
	private int id,areaId,districtId;
	private String name,phone,email,website,address;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BloodBank(int id, int areaId, int districtId, String name,
			String phone, String email, String website) {
		super();
		this.id = id;
		this.areaId = areaId;
		this.districtId = districtId;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.website = website;
	}
	public BloodBank(){}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}

}
