package com.cyan.valothaki.entity;

public class Question {
	private int id,userId;
	private String description,date,time,userName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Question(){}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Question(int id, int userId, String description, String date,
			String time) {
		super();
		this.id = id;
		this.userId = userId;
		this.description = description;
		this.date = date;
		this.time = time;
	}

}
