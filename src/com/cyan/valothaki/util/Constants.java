package com.cyan.valothaki.util;

import com.cyan.valothaki.R;


public interface Constants {

	int[] COLORS = { R.color.Red500, R.color.Blue500, R.color.Cyan500,
			R.color.LightGreen500, R.color.BlueGrey500, R.color.Grey500,
			R.color.Orange500, R.color.Teal500 };
	int[] COLORS1 = { R.color.Red400, R.color.Blue400, R.color.Cyan400,
			R.color.LightGreen400, R.color.BlueGrey400, R.color.Grey400,
			R.color.Orange400, R.color.Teal400 };
	String[] WEEK_DAYS = { "Sunday", "Monday", "Tuesday", "Wednesday",
			"Thursday", "Friday", "Saturday" };
	String ID = "id";
	String PAGE = "page";
	String HOSPITAL = "hospital";
	String BLOODBANK = "blood bank";
	String AMBULANCE = "ambulance";

}
