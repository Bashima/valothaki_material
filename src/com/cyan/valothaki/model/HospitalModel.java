package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.cyan.valothaki.dao.HospitalDao;
import com.cyan.valothaki.entity.Hospital;
import com.cyan.valothaki.item.SingleItem;


public class HospitalModel {
	public static ArrayList<Hospital> hospitals;
	public static ArrayList<SingleItem> items;
	
	public static void loadDataModel(Context context,int district) {
		HospitalDao hospitalDao = new HospitalDao(context,district);
		hospitals = new ArrayList<Hospital>();
		items = new ArrayList<SingleItem>();
		//ArrayList<String> districts = districtDao.DemoPopulate();
		ArrayList<Hospital> hospitals = hospitalDao.PopulateHospital();
		Log.d("size: ",String.valueOf(hospitals.size()));
		for (int i = 0; i < hospitals.size(); i++) {

			Hospital temp = hospitals.get(i);
			
			//hospitals.add(temp);
			
			items.add(new SingleItem(i + 1, temp.getName()));
			Log.d("name: ", temp.getName()+" "+String.valueOf(hospitals.size())+" "+String.valueOf(i));
			

		}
		Log.d("name: ", "done");
		
	}
	/*public static Hospital getbyId(int id) {
		for (Hospital item : hospitals) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}*/
	
	public static SingleItem getbyId(int id) {
		for (SingleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
