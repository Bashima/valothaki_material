package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;

import com.cyan.valothaki.dao.DistrictDao;
import com.cyan.valothaki.item.SingleItem;

public class ChamberModel{

	public static ArrayList<SingleItem> items;
	public static void loadDataModel(Context context) {
//		tamzeed ei khane appintment request dao ta thakbe
		DistrictDao districtDao = new DistrictDao(context);
		items = new ArrayList<SingleItem>();
//		also change the line below
		ArrayList<String> patients = districtDao.DemoPopulate();
		for (int i = 0; i < patients.size(); i++) {

			String temp = patients.get(i);
			items.add(new SingleItem(i + 1, temp));

		}
		
	}
	public static SingleItem getbyId(int id) {
		for (SingleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
