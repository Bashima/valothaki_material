package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.dao.AppointmentDao;
import com.cyan.valothaki.entity.Appointment;
import com.cyan.valothaki.item.DoubleItem;
import com.cyan.valothaki.item.SingleItem;

public class ShowPatientModel{

	public static ArrayList<DoubleItem> items;
	public static ArrayList<Appointment> appointments;
	public static void loadDataModel(Context context) {

		////////////////////////////////
		
		
		AppointmentDao docDao = new AppointmentDao(context,Integer.parseInt(ConstantsUser.docId));
//		also change the line below
		appointments=docDao.PopulateAppointment();
		

		
		/////////////////////////////////
		items = new ArrayList<DoubleItem>();
		for (int i = 0; i < appointments.size(); i++) {
			Appointment temp = appointments.get(i);
			items.add(new DoubleItem(temp.getAppointmentId(), temp.getUserName(), temp.getDate()+"\n"+temp.getTime()));
			Log.d("added here", String.valueOf(temp.getAppointmentId()));
		}
		
	}
	
	
	
	public static DoubleItem getbyId(int id) {
		for (DoubleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
