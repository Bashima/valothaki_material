package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;

import com.cyan.valothaki.dao.SpecialityDao;
import com.cyan.valothaki.item.SingleItem;

public class DoctorSpecialityModel {
	public static ArrayList<SingleItem> items;

	public static void loadDataModel(Context context) {
		SpecialityDao districtDao = new SpecialityDao(context);
		items = new ArrayList<SingleItem>();
		ArrayList<String> districts = districtDao.Populate();
		for (int i = 0; i < districts.size(); i++) {

			String temp = districts.get(i);
			items.add(new SingleItem(i + 1, temp));

		}

	}

	public static SingleItem getbyId(int id) {
		for (SingleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
