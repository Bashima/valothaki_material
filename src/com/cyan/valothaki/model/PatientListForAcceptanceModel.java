package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.dao.AppointmentDao;
import com.cyan.valothaki.dao.DistrictDao;
import com.cyan.valothaki.dao.DoctorAssistantDao;
import com.cyan.valothaki.entity.Appointment;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.webservice.DoctorPendingRequest;

public class PatientListForAcceptanceModel{

	public static ArrayList<SingleItem> items;
	public static ArrayList<Appointment> appointments;
	public static void loadDataModel(Context context) {
//		tamzeed ei khane appintment request dao ta thakbe
		
		AppointmentDao docDao = new AppointmentDao(context,Integer.parseInt(ConstantsUser.docId));
		items = new ArrayList<SingleItem>();
//		also change the line below
		appointments=docDao.PopulateAppointment();
		
//		ArrayList<String> patients = districtDao.DemoPopulate();
		for (int i = 0; i < appointments.size(); i++) {

			Appointment temp = appointments.get(i);
			items.add(new SingleItem(temp.getAppointmentId(), temp.getUserName()));

		}
		
	}
	public static SingleItem getbyId(int id) {
		for (SingleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
