package com.cyan.valothaki.model;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.cyan.valothaki.dao.AmbulanceDao;
import com.cyan.valothaki.dao.BloodBankDao;
import com.cyan.valothaki.dao.DistrictDao;
import com.cyan.valothaki.dao.DoctorDao;
import com.cyan.valothaki.entity.Doctor;
import com.cyan.valothaki.item.SingleItem;

public class SingleItemModel {

	public static ArrayList<SingleItem> items;
	public static ArrayList<Doctor> docs;

	public static void loadDataModel(Context context) {
		DistrictDao districtDao = new DistrictDao(context);
		items = new ArrayList<SingleItem>();
		ArrayList<String> districts = districtDao.DemoPopulate();
		for (int i = 0; i < districts.size(); i++) {

			String temp = districts.get(i);
			items.add(new SingleItem(i + 1, temp));

		}

	}

	public static void loadDataModel(Context context, int district, int flag) {
		ArrayList<String> populated = null;

		if (flag == 1) {
			// bloodbankda
			Log.d("sing id: ", String.valueOf(district));
			BloodBankDao bloodBankDao = new BloodBankDao(context, district);
			items = new ArrayList<SingleItem>();
			populated = bloodBankDao.PopulateBloodBank();

		}

		else if (flag == 2) {
			Log.d("sing id: ", String.valueOf(district));
			// /ambulance dao
			AmbulanceDao dao = new AmbulanceDao(context, district);
			items = new ArrayList<SingleItem>();
			populated = dao.PopulateAmbulance();

		}

		else if (flag == 3) {
			DoctorDao dao = new DoctorDao(context);
			docs = dao.getDocList(district, flag);

		}

		for (int i = 0; i < populated.size(); i++) {

			String temp = populated.get(i);
			items.add(new SingleItem(i + 1, temp));

		}

	}

	public static void loadDataModel(Context context, int district, int flag,
			int spec) {
//		ArrayList<String> populated = null;
		DoctorDao dao = new DoctorDao(context);
		docs = dao.getDocList(district, spec);
		items = new ArrayList<SingleItem>();
		for (int i = 0; i < docs.size(); i++) {
			Doctor temp = docs.get(i);
			Log.d("dpc:", temp.getName());
			items.add(new SingleItem(temp.getUserId(), temp.getName()));
		}

	}

	public static SingleItem getbyId(int id) {
		for (SingleItem item : items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

}
