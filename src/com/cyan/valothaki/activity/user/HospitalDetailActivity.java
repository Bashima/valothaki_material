package com.cyan.valothaki.activity.user;

import it.gmariotti.cardslib.library.view.CardListView;
import android.widget.RatingBar.OnRatingBarChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.adapter.ExpandableListAdapter;
import com.cyan.valothaki.dao.DoctorDao;
import com.cyan.valothaki.dao.HospitalDetailDao;
import com.cyan.valothaki.dao.SpecialityDao;
import com.cyan.valothaki.entity.Doctor;
import com.cyan.valothaki.entity.Hospital;
import com.cyan.valothaki.webservice.UserPostRating;
import com.shamanland.fab.FloatingActionButton;
import com.shamanland.fab.ShowHideOnScroll;

public class HospitalDetailActivity extends Activity {

	public int id;
	public TextView mNameTv;
	public TextView mRateTv;
	public TextView mVoteTv;
	public TextView mNumberTv;
	public TextView mAddressTv;
	public ImageButton mCallBt;
	public CardListView listView;
	public Context mContext;
	public Hospital hospital;
	public String rate="1";

	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hospital_detail);
		//id = getIntent().getIntExtra(Constants.ID, 0);
		Intent intent = getIntent();
		if (null != intent) {
			//district=intent.getIntExtra("district", 0);
			String d=intent.getStringExtra("HospitalId");
			id=Integer.parseInt(d);
			Log.d("hospital is: ", String.valueOf(id));
			
		
		}

		mContext = this;
		mNameTv = (TextView) findViewById(R.id.tv_name);
		mRateTv = (TextView) findViewById(R.id.tv_rating);
		mVoteTv = (TextView) findViewById(R.id.tv_vote);
		mNumberTv = (TextView) findViewById(R.id.tv_number);
		mAddressTv = (TextView) findViewById(R.id.tv_hospital_name);
		mCallBt = (ImageButton) findViewById(R.id.bt_call);
		
		HospitalDetailDao dao=new HospitalDetailDao(this, id);
		hospital=dao.HospitalDetail();

		mNameTv.setText(hospital.getName());
		mRateTv.setText("4");
		mVoteTv.setText("100");
		mNumberTv.setText(hospital.getPhone());
		mAddressTv.setText(hospital.getAddress());
		mCallBt.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + mNumberTv.getText()));
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("Calling a Phone Number", "Call failed",
							activityException);
				}
			}
		});

		// get the listview
		expListView = (ExpandableListView) findViewById(R.id.cardlist);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

		expListView.setOnTouchListener(new ShowHideOnScroll(fab));

		fab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onRateClick();
			}
		});

		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		expListView.startAnimation(animation);
		// preparing list data
		prepareListData();

		ExpandableListAdapter listAdapter = new ExpandableListAdapter(this,
				listDataHeader, listDataChild, HospitalDetailActivity.this);

		// setting list adapter
		expListView.setAdapter(listAdapter);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hospital_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void prepareListData() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();

		// Adding child data
//		listDataHeader.add("Top 250");
//		listDataHeader.add("Now Showing");
//		listDataHeader.add("Coming Soon..");
//		listDataHeader.add("Top 250");
//		listDataHeader.add("Now Showing");
//		listDataHeader.add("Coming Soon..");
//		listDataHeader.add("Top 250");
//		listDataHeader.add("Now Showing");
//		listDataHeader.add("Coming Soon..");
//		listDataHeader.add("Top 250");
//		listDataHeader.add("Now Showing");
//		listDataHeader.add("Coming Soon..");
//		
		ArrayList<Doctor> doctorList=hospital.getDocs();
		SpecialityDao dao=new SpecialityDao(mContext);
		ArrayList<String> specList=dao.Populate();
//		for(int i=0;i<doctorList.size();i++)
//		{
//			Doctor doc=doctorList.get(i);
//			List<String> tempList = new ArrayList<String>();
//			String tempArg=doc.getUserId()+"#"+doc.getSpecialization()+"#"+doc.getPhone();
//			tempList.add(tempArg);
//			listDataHeader.add(doc.getName());
//			listDataChild.put(listDataHeader.get(i),tempList); 
//			
//		}
		DoctorDao docDao=new DoctorDao(mContext);
		for(int i=0;i<specList.size();i++)
		{
			List<String> tempList = new ArrayList<String>();
			ArrayList<Doctor> doctorListSpec=docDao.PopulateSpec(i+1, hospital.getId(), 1);
			listDataHeader.add(specList.get(i));
			for(int j=0;j<doctorListSpec.size();j++)
			{
				Doctor tem=doctorListSpec.get(j);
				String tempArg=tem.getName()+"\n"+tem.getEmail()+"\n"+tem.getPhone();
				tempList.add(tempArg);
				
			}
			listDataChild.put(listDataHeader.get(i),tempList);
			
			
		}

//		// Adding child data
//		List<String> top250 = new ArrayList<String>();
//		top250.add("The Shawshank Redemption");
//
//		List<String> nowShowing = new ArrayList<String>();
//		nowShowing.add("The Conjuring");
//
//		List<String> comingSoon = new ArrayList<String>();
//		comingSoon.add("2 Guns");
//
//		listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
//		listDataChild.put(listDataHeader.get(1), nowShowing);
//		listDataChild.put(listDataHeader.get(2), comingSoon);
//		listDataChild.put(listDataHeader.get(3), top250); // Header, Child data
//		listDataChild.put(listDataHeader.get(4), nowShowing);
//		listDataChild.put(listDataHeader.get(5), comingSoon);
//		listDataChild.put(listDataHeader.get(6), top250); // Header, Child data
//		listDataChild.put(listDataHeader.get(7), nowShowing);
//		listDataChild.put(listDataHeader.get(8), comingSoon);
//		listDataChild.put(listDataHeader.get(9), top250); // Header, Child data
//		listDataChild.put(listDataHeader.get(10), nowShowing);
//		listDataChild.put(listDataHeader.get(11), comingSoon);
	}

	public void onRateClick() {
		final Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_rating);
		mDialog.setCancelable(true);

		  
		Button mRateButton = (Button) mDialog.findViewById(R.id.bt_ok);
		RatingBar ratingBar= (RatingBar) mDialog.findViewById(R.id.ratingBar1);
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
				boolean fromUser) {
				Log.d("rate: ", String.valueOf(rating));
				rate=String.valueOf(rating);
	 
			}
		});
		//ratingBar.setOnRatingBarChangeListener(listener)
		// if button is clicked, close the custom dialog
		mRateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String arg=rate+"$"+id+"$"+1;
				new UserPostRating(arg).execute();
				
				mDialog.dismiss();
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}

}
