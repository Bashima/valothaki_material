package com.cyan.valothaki.activity.user;

import java.io.File;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.cyan.valothaki.R;
import com.mikhaellopez.circularimageview.CircularImageView;

/*
 * This activity is the menu. It contains 7 button each of which lead to 
 * profile, hospital, doctor, appointment, ambulance, blood bank and forum correspondingly
 * This activity is called by SignInActivity or SignUpActivity  
 */
public class DashboardActivity extends Activity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle) This method only
	 * inflate the layout
	 */
	
	public CircularImageView mProfile;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		
		mProfile = (CircularImageView) findViewById(R.id.profile);
		File file = new File(
 				this.getExternalFilesDir(Environment.DIRECTORY_DCIM),
 				"profilepic.png");
 		if (file.exists()) {
 			Drawable d = Drawable.createFromPath(file.getPath());
 			mProfile.setImageDrawable(d);
 		}
		mProfile.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent mIntent = new Intent(DashboardActivity.this,
							UserProfileActivity.class);
					startActivity(mIntent);
				} catch (ActivityNotFoundException activityException) {
					
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu) This
	 * method is responsible for inflatin menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dashboard, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 * This method is responsible for going to setting and help menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_help) {
			Intent mIntent = new Intent(DashboardActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * Action handler for hospital button
	 */
	public void onHospitalClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				DistrictHospitalActivity.class);
		startActivity(mIntent);
	}

	/*
	 * Action handler for ambulance button
	 */
	public void onAmbulanceClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				DistrictAmbulanceActivity.class);
		startActivity(mIntent);
	}

	/*
	 * Action handler for blood bank button
	 */
	public void onBloodClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				DistrictBloodBankActivity.class);
		startActivity(mIntent);
	}
	
	public void onDoctorClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				DoctorSpecialityActivity.class);
		startActivity(mIntent);
	}
	
	public void onForumClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				ForumQuestionListActivity.class);
		startActivity(mIntent);
	}
	
	public void onAppointmentClick(View v) {
		Intent mIntent = new Intent(DashboardActivity.this,
				AppointmentActivity.class);
		startActivity(mIntent);
	}
}
