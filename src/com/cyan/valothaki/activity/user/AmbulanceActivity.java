package com.cyan.valothaki.activity.user;

/*
 * AmbulanceActivity is the class where the list of the ambulances are shown.
 * This class extends StickyListAdapterActivity 
 * This class is being called by DistrictAmbulanceActivity.
 */
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.StickyListBaseActivity;
import com.cyan.valothaki.adapter.DoubleTextAdapter;
import com.cyan.valothaki.dao.AmbulanceDao;
import com.cyan.valothaki.entity.Ambulance;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.SingleItemModel;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.webservice.UserSyncAmbulance;

public class AmbulanceActivity extends StickyListBaseActivity {

	public List<SingleItem> mAmbulances;
	int district;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.StickyListBaseActivity#dataLoad() This
	 * dataLoad() method overrides the abstract method in StickyListBaseActivity
	 * It loads data from the Ambulance Table of Database after filtering the
	 * ambulances of the selected district.
	 */
	DoubleTextAdapter mAdapter;
	Context mContext;
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		
		EditText editText=(EditText) findViewById(R.id.et_search);
		editText.addTextChangedListener(new TextWatcher() {
			  
		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        System.out.println("Text ["+s+"]");
		        mAdapter.getFilter().filter(s.toString());                           
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		         
		    }
		     
		    @Override
		    public void afterTextChanged(Editable s) {
		    }
		});
		
		

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			new UserSyncAmbulance(mContext, district).execute();
			Log.d("dist: ", String.valueOf(district));
			return true;
		} else if (id == R.id.action_help) {
			Intent mIntent = new Intent(AmbulanceActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void dataLoad() {
		Intent intent = getIntent();
		String res = intent.getStringExtra("district");
		Log.d("ambu", res);
		district = Integer.parseInt(res);
		mContext = this;
		SingleItemModel.loadDataModel(mContext, district, 2);
		int mNumberOfTotalAmbulances = SingleItemModel.items.size();
		mAmbulances = SingleItemModel.items
				.subList(0, mNumberOfTotalAmbulances);

		StickyListHeadersListView mStickyList = (StickyListHeadersListView) findViewById(R.id.ls_hospital);
		 mAdapter = new DoubleTextAdapter(this,
				R.layout.row_double_text_left_circle, mAmbulances);
		Animation mAnimation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		mStickyList.startAnimation(mAnimation);
		mStickyList.setOnItemClickListener(this);
		mStickyList.setAdapter((StickyListHeadersAdapter) mAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.StickyListBaseActivity#onItemClick(android
	 * .widget.AdapterView, android.view.View, int, long) In this method dialog
	 * is shown on click of an ambulance item of the list. In this dialog all
	 * the information along with the calling facility is implemented.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_ambulance);
		LinearLayout mLinearLayout = (LinearLayout) mDialog
				.findViewById(R.id.color_layout);
		TextView mName = (TextView) mDialog.findViewById(R.id.tv_name);
		mName.setText(mAmbulances.get(position).text);
		int pId = mAmbulances.get(position).id;
		AmbulanceDao dao = new AmbulanceDao(this, pId);
		Ambulance ambu = dao.getAmbulance(pId);

		TextView mRate = (TextView) mDialog.findViewById(R.id.tv_rate);
		mRate.setText("Rate: " + ambu.getFee() + " BDT");

		final TextView mNumber = (TextView) mDialog
				.findViewById(R.id.tv_number);
		mNumber.setText(ambu.getPhone());

		int colorIndex = (int) ((position + 1) % Constants.COLORS.length);
		mLinearLayout.setBackgroundColor(getResources().getColor(
				Constants.COLORS[colorIndex]));

		ImageButton mCallBt = (ImageButton) mDialog.findViewById(R.id.bt_call);

		mCallBt.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + mNumber.getText()));
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("Calling a Phone Number", "Call failed",
							activityException);
				}
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}

}
