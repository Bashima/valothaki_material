package com.cyan.valothaki.activity.user;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.cyan.valothaki.R;
import com.cyan.valothaki.fragment.CalendarDayFragment;
import com.cyan.valothaki.fragment.CalendarMonthFragment;
import com.cyan.valothaki.fragment.CalendarMonthFragment.OnDateSelectListener;
import com.cyan.valothaki.fragment.CalendarWeekFragment;

public class AppointmentActivity extends FragmentActivity implements
		OnDateSelectListener {

	public static long eventId = 0;
	public static int viewI = 0;
	public static int viewT = 0;
	public static long clientId = 0;
	public static boolean popup = false;

	private Fragment fragment = null;
	private Bundle bundle = new Bundle();
	// private Event event;
	public static String source;

	// private DatabaseHelper databaseHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_appointment);
		getActionBar().setIcon(
				new ColorDrawable(getResources().getColor(
						android.R.color.transparent)));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

//		Bundle extras = getIntent().getExtras();
//		Log.d("got", extras.toString());
		// source = getIntent().getStringExtra(Constants.SOURCE);
		// if (source.equals(Constants.CLIENT)) {
		// clientId = getIntent().getLongExtra(Constants.CLIENT_ID, -1);
		// }
		// if (extras != null) {
		// eventId = getIntent().getLongExtra(Constants.EVENT_ID, -1);
		// } else {
		// eventId = -1;
		// }

		// if (eventId != -1) {
		// showDialog(0);
		// } else {
		viewI = 0;
		viewT = 0;
		ArrayList<String> calendarViewList = new ArrayList<String>();
		calendarViewList.add("Day View");
		calendarViewList.add("Week View");
		calendarViewList.add("Month View");

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.row_spinner, R.id.tv_choice, calendarViewList);

		getActionBar().setListNavigationCallbacks(adapter,
				new ActionBar.OnNavigationListener() {

					@Override
					public boolean onNavigationItemSelected(int itemPosition,
							long itemId) {
						switch (itemPosition) {
						// Week View
						case 0:
							Fragment dayFragment = new CalendarDayFragment();
							setFragment(dayFragment);
							return true;
							// Month View
						case 1:
							Fragment weekFragment = new CalendarWeekFragment();
							setFragment(weekFragment);
							return true;
							// Day View
						case 2:
							Fragment monthFragment = new CalendarMonthFragment();
							setFragment(monthFragment);
							return true;
						default:
							return false;
						}
					}
				});

		fragment = new CalendarDayFragment();
		fragment.setArguments(bundle);
		setFragment(fragment);

		// }
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		super.onCreateDialog(id);

		// Build the dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Reminder");
		// alert.setMessage(getData(eventId));
		alert.setCancelable(false);

		alert.setPositiveButton("Dismiss",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
				});

		AlertDialog dlg = alert.create();
		return dlg;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.appointment, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			Intent i = new Intent(this, AppointmentAddActivity.class);
			// i.putExtra(Constants.TYPE, Constants.ADD_NEW);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void setFragment(Fragment fragment) {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.content_frame, fragment);
		transaction.commit();
	}

	@Override
	public void onDateSelected() {
		getActionBar().setSelectedNavigationItem(0);
	}

	// private String getData(long eventId) {
	// try {
	// Dao<Event, Long> eventDao = getHelper().getDao(Event.class);
	// event = eventDao.queryForId(eventId);
	// long caseId = event.getCaseId();
	//
	// Dao<Case, Long> caseDao = getHelper().getDao(Case.class);
	// Case cases = caseDao.queryForId(caseId);
	// String name = cases.getName();
	// return "You have an event named " + event.getEventName()
	// + " of case: " + name;
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	// return "You have an event.";
	// }

	// private DatabaseHelper getHelper() throws SQLException {
	// if (databaseHelper == null) {
	// databaseHelper = OpenHelperManager.getHelper(this,
	// DatabaseHelper.class);
	// }
	// return databaseHelper;
	// }
	public void onBackPressed() {
		finish();
	}

}
