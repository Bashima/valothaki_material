package com.cyan.valothaki.activity.user;

import java.util.List;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.SingleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.DistrictBloodbankModel;

/*
 * This activity extends the DistrictBaseActivity and overrides loadData and
 * onListItemClick method. This activity is called from Dashboard activity by
 * onBloodClick metod.
 */
public class DistrictBloodBankActivity extends DistrictBaseActivity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.DistrictBaseActivity#loadData() This
	 * method loads data from database District and BloodBank table.
	 */
	public void loadData() {
		DistrictBloodbankModel.loadDataModel(mContext);
		int numberOfTotalBloodBank = DistrictBloodbankModel.items.size();
		List<SingleItem> mBloodBanks = DistrictBloodbankModel.items.subList(0,
				numberOfTotalBloodBank);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);

		SingleTextAdapter singleAdapter = new SingleTextAdapter(this,
				R.layout.row_single_text_left_circle, mBloodBanks);
		setListAdapter(singleAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.DistrictBaseActivity#onListItemClick(android
	 * .widget.ListView, android.view.View, int, long) This method is action
	 * handler for on list item click starts BloodBankActivity. 
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent i;
		i = new Intent(DistrictBloodBankActivity.this, BloodBankActivity.class);
		i.putExtra("district", String.valueOf(id+1));
		startActivity(i);
	}
}
