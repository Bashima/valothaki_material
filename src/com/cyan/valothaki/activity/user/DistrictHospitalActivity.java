package com.cyan.valothaki.activity.user;

import java.util.List;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.SingleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.DistrictHospitalModel;

/*
 * This activity extends the DistrictBaseActivity and overrides loadData and
 * onListItemClick method. This activity is called from Dashboard activity by
 * onHospitalClick metod.
 */
public class DistrictHospitalActivity extends DistrictBaseActivity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.DistrictBaseActivity#loadData() This
	 * method loads data from database District and Hospital table.
	 */
	public void loadData() {
		Log.d("id district", "loading");
		DistrictHospitalModel.loadDataModel(mContext);
		int numberOfTotalHospital = DistrictHospitalModel.items.size();
		List<SingleItem> clients = DistrictHospitalModel.items.subList(0,
				numberOfTotalHospital);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);

		SingleTextAdapter singleAdapter = new SingleTextAdapter(this,
				R.layout.row_single_text_left_circle, clients);
		setListAdapter(singleAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.DistrictBaseActivity#onListItemClick(android
	 * .widget.ListView, android.view.View, int, long) This method is action
	 * handler for on list item click which starts HospitalActivity.
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent i;
		Log.d("id district", String.valueOf(id+1));
		i = new Intent(DistrictHospitalActivity.this, HospitalActivity.class);
		//i.putExtra(Constants.ID, id);
		i.putExtra("district",String.valueOf(id+1));
		
		startActivity(i);
	}
}
