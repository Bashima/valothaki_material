package com.cyan.valothaki.activity.user;

import it.gmariotti.cardslib.library.view.CardListView;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.adapter.ChamberTextAdapter;
import com.cyan.valothaki.dao.DoctorDao;
import com.cyan.valothaki.entity.Chamber;
import com.cyan.valothaki.entity.Doctor;
import com.cyan.valothaki.model.DistrictAmbulanceModel;
import com.shamanland.fab.FloatingActionButton;
import com.shamanland.fab.ShowHideOnScroll;

public class DoctorDetailActivity extends Activity implements
		AdapterView.OnItemClickListener {

	public int id;
	public TextView mNameTv;
	public TextView mDegreeTv;
	public TextView mRateTv;
	public TextView mVoteTv;
	public TextView mNumberTv;
	public TextView mHospitalTv;
	public ImageButton mCallBt;
	public CardListView listView;
	public Context mContext;

	ListView expListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor_detail);
		// id = getIntent().getIntExtra(Constants.ID, 0);
		Intent intent = getIntent();
		if (null != intent) {
			id = intent.getIntExtra("DoctorId", 0);

		}

		DoctorDao dao = new DoctorDao(this);
		Doctor doctor = dao.getDoctor(id);
		;

		mContext = this;
		mNameTv = (TextView) findViewById(R.id.tv_name);
		mDegreeTv = (TextView) findViewById(R.id.tv_degree);
		mRateTv = (TextView) findViewById(R.id.tv_rating);
		mVoteTv = (TextView) findViewById(R.id.tv_vote);
		mNumberTv = (TextView) findViewById(R.id.tv_number);
		mHospitalTv = (TextView) findViewById(R.id.tv_hospital_name);
		mCallBt = (ImageButton) findViewById(R.id.bt_call);

		mNameTv.setText(doctor.getName());
		mRateTv.setText("5");
		mVoteTv.setText("100");
		mNumberTv.setText(doctor.getPhone());
		mHospitalTv.setText(doctor.getHospital());
		mDegreeTv.setText(doctor.getQualification() + "\n"
				+ doctor.getSpecialization());
		mCallBt.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + mNumberTv.getText()));
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("Calling a Phone Number", "Call failed",
							activityException);
				}
			}
		});

		DistrictAmbulanceModel.loadDataModel(mContext);
//		int numberOfTotalHospital = DistrictAmbulanceModel.items.size();
//		List<SingleItem> clients = DistrictAmbulanceModel.items.subList(0,
//				numberOfTotalHospital);

		List<Chamber> chambers = dao.getChamber(id);

		// get the listview
		expListView = (ListView) findViewById(R.id.cardlist);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

		expListView.setOnTouchListener(new ShowHideOnScroll(fab));

		fab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onRateClick();
			}
		});

		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		expListView.startAnimation(animation);
		expListView.setOnItemClickListener(this);
		// //
		ChamberTextAdapter listAdapter = new ChamberTextAdapter(this,
				R.layout.row_card, chambers);

		// setting list adapter
		expListView.setAdapter(listAdapter);

		Log.d("si", String.valueOf(chambers.size()));
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hospital_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_sync) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}

	public void onRateClick() {
		final Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_rating_doc);
		mDialog.setCancelable(true);

		Button mRateButton = (Button) mDialog.findViewById(R.id.bt_ok);
		// if button is clicked, close the custom dialog
		mRateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(
				DoctorDetailActivity.this);
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
				DoctorDetailActivity.this, android.R.layout.select_dialog_item);
		arrayAdapter.add("Call chamber");
		arrayAdapter.add("Take an appointment");

		builderSingle.setAdapter(arrayAdapter,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String strName = arrayAdapter.getItem(which);
						if (strName.equals("Call chamber")) {
							try {
								Intent callIntent = new Intent(
										Intent.ACTION_CALL);
								callIntent.setData(Uri.parse("tel:"
										+ mNumberTv.getText()));
								startActivity(callIntent);
							} catch (ActivityNotFoundException activityException) {
								Log.e("Calling a Phone Number", "Call failed",
										activityException);
							}
						} else if (strName.equals("Take an appointment")) {
							Intent mIntent = new Intent(
									DoctorDetailActivity.this,
									DashboardActivity.class);
							startActivity(mIntent);
						} else {
							dialog.dismiss();
						}
					}
				});
		builderSingle.show();
	}
}
