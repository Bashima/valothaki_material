package com.cyan.valothaki.activity.user;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.StickyListBaseActivity;
import com.cyan.valothaki.adapter.DoubleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.HospitalModel;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.webservice.UserSyncHospital;

public class HospitalActivity extends StickyListBaseActivity {

	Context mContext;
	int district;
	ArrayAdapter<String> adapter;
	DoubleTextAdapter mAdapter;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		
		EditText editText=(EditText) findViewById(R.id.et_search);
		editText.addTextChangedListener(new TextWatcher() {
			  
		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        System.out.println("Text ["+s+"]");
		        mAdapter.getFilter().filter(s.toString());                           
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		         
		    }
		     
		    @Override
		    public void afterTextChanged(Editable s) {
		    }
		});
		
		

	}

	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		Log.d("AutocompleteContacts", "onItemSelected() position " + position);
	}

	protected void onResume() {
		super.onResume();
	}

	protected void onDestroy() {
		super.onDestroy();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			new UserSyncHospital(mContext, district).execute();
			Log.d("dist: ", String.valueOf(district));
			Intent mIntent = new Intent(HospitalActivity.this,
					HospitalActivity.class);
			mIntent.putExtra("district",String.valueOf(district));
			startActivity(mIntent);
			return true;
		} else if (id == R.id.action_help) {
			Intent mIntent = new Intent(HospitalActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * public HospitalActivity() { // TODO Auto-generated constructor stub
	 * Bundle b = getIntent().getExtras(); district = b.getInt("district");
	 * Log.d("district is: ", String.valueOf(district)); }
	 */

	/*
	 * public void dataLoad() { mContext = this;
	 * SingleItemModel.loadDataModel(mContext); int mNumberOfTotalHospitals =
	 * SingleItemModel.items.size(); List<SingleItem> clients =
	 * SingleItemModel.items.subList(0, mNumberOfTotalHospitals);
	 * 
	 * StickyListHeadersListView mStickyList = (StickyListHeadersListView)
	 * findViewById(R.id.ls_hospital); DoubleTextAdapter mAdapter = new
	 * DoubleTextAdapter(this, R.layout.row_double_text_left_circle, clients);
	 * Animation mAnimation = AnimationUtils.loadAnimation(this,
	 * R.anim.up_from_bottom_slow); mStickyList.startAnimation(mAnimation);
	 * mStickyList.setOnItemClickListener(this);
	 * mStickyList.setAdapter((StickyListHeadersAdapter) mAdapter); }
	 */
	public void dataLoad() {

		Intent intent = getIntent();
		if (null != intent) {
			// district=intent.getIntExtra("district", 0);
			String d = intent.getStringExtra("district");
			district = Integer.parseInt(d);
			Log.d("district is: ", String.valueOf(district));

		}
		mContext = this;
		HospitalModel.loadDataModel(mContext, district);
		int mNumberOfTotalHospitals = HospitalModel.items.size();
		List<SingleItem> clients = HospitalModel.items.subList(0,
				mNumberOfTotalHospitals);
		Log.d("size: ", String.valueOf(mNumberOfTotalHospitals));

		StickyListHeadersListView mStickyList = (StickyListHeadersListView) findViewById(R.id.ls_hospital);
		mAdapter = new DoubleTextAdapter(this,
				R.layout.row_double_text_left_circle, clients);
		Animation mAnimation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		mStickyList.startAnimation(mAnimation);
		mStickyList.setOnItemClickListener(this);
		mStickyList.setAdapter((StickyListHeadersAdapter) mAdapter);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent mIntent = new Intent(HospitalActivity.this,
				HospitalDetailActivity.class);
		Log.d("hospital id: ", String.valueOf(id));
		mIntent.putExtra("HospitalId", String.valueOf(id + 1));
		startActivity(mIntent);
	}
}
