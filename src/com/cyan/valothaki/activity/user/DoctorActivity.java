package com.cyan.valothaki.activity.user;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.StickyListBaseActivity;
import com.cyan.valothaki.adapter.DoubleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.SingleItemModel;

public class DoctorActivity extends StickyListBaseActivity {

	public List<SingleItem> mDoctors;
	Context mContext;
	int districtId, specId;

	DoubleTextAdapter mAdapter;
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		
		EditText editText=(EditText) findViewById(R.id.et_search);
		editText.addTextChangedListener(new TextWatcher() {
			  
		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        System.out.println("Text ["+s+"]");
		        mAdapter.getFilter().filter(s.toString());                           
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		         
		    }
		     
		    @Override
		    public void afterTextChanged(Editable s) {
		    }
		});
		
		

	}

	public void dataLoad() {

		mContext = this;
		Intent intent = getIntent();
		if (null != intent) {
			// districtId=intent.getIntExtra("DistrictId",0);
			specId = intent.getIntExtra("SpectId", 0);

		}
		SingleItemModel.loadDataModel(mContext, districtId, specId, specId);
		int mNumberOfTotalDoctors = SingleItemModel.items.size();
		mDoctors = SingleItemModel.items.subList(0, mNumberOfTotalDoctors);

		StickyListHeadersListView mStickyList = (StickyListHeadersListView) findViewById(R.id.ls_hospital);
		mAdapter = new DoubleTextAdapter(this,
				R.layout.row_double_text_left_circle, mDoctors);
		Animation mAnimation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		mStickyList.startAnimation(mAnimation);
		mStickyList.setOnItemClickListener(this);
		mStickyList.setAdapter((StickyListHeadersAdapter) mAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent mIntent;
		SingleItem tempDoc = mDoctors.get(position);
		int id1 = tempDoc.id;
		mIntent = new Intent(DoctorActivity.this, DoctorDetailActivity.class);
		mIntent.putExtra("DoctorId", id1);
		Log.d("docid: ", String.valueOf(id1));
		startActivity(mIntent);

	}

}
