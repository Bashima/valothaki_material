package com.cyan.valothaki.activity.user;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.StickyListBaseActivity;
import com.cyan.valothaki.adapter.DoubleTextAdapter;
import com.cyan.valothaki.dao.BloodBankDao;
import com.cyan.valothaki.entity.BloodBank;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.SingleItemModel;
import com.cyan.valothaki.util.Constants;
import com.cyan.valothaki.webservice.UserSyncBloodBank;

/*
 * BloodBankActivity is the class where the list of the blood banks are shown.
 * This class extends StickyListAdapterActivity 
 * This class is being called by DistrictBloodBankActivity.
 */
public class BloodBankActivity extends StickyListBaseActivity {

	public List<SingleItem> mAmbulances;
	int district;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.StickyListBaseActivity#dataLoad() This
	 * dataLoad() method overrides the abstract method in StickyListBaseActivity
	 * It loads data from the BloodBank Table of Database after filtering the
	 * blood banks of the selected district.
	 */
	Context mContext;
	DoubleTextAdapter adapter;
	
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		
		EditText editText=(EditText) findViewById(R.id.et_search);
		editText.addTextChangedListener(new TextWatcher() {
			  
		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        System.out.println("Text ["+s+"]");
		        adapter.getFilter().filter(s.toString());                           
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		         
		    }
		     
		    @Override
		    public void afterTextChanged(Editable s) {
		    }
		});
		
		

	}

	public void dataLoad() {
		Intent intent = getIntent();
		String d = intent.getStringExtra("district");
		this.district = Integer.parseInt(d);
		mContext = this;
		SingleItemModel.loadDataModel(mContext, district, 1);
		int numberOfTotalDoctor = SingleItemModel.items.size();
		mAmbulances = SingleItemModel.items.subList(0, numberOfTotalDoctor);

		StickyListHeadersListView stickyList = (StickyListHeadersListView) findViewById(R.id.ls_hospital);
		adapter = new DoubleTextAdapter(this,
				R.layout.row_double_text_left_circle, mAmbulances);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		stickyList.startAnimation(animation);
		stickyList.setOnItemClickListener(this);
		stickyList.setAdapter((StickyListHeadersAdapter) adapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.StickyListBaseActivity#onItemClick(android
	 * .widget.AdapterView, android.view.View, int, long) In this method dialog
	 * is shown on click of an vlood bank item of the list. In this dialog all
	 * the information along with the calling facility is implemented.
	 */

	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			new UserSyncBloodBank(mContext, district).execute();
			Log.d("dist: ", String.valueOf(district));
			return true;
		} else if (id == R.id.action_help) {
			Intent mIntent = new Intent(BloodBankActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_blood_bank);
		LinearLayout mLinearLayout = (LinearLayout) mDialog
				.findViewById(R.id.color_layout);
		TextView mName = (TextView) mDialog.findViewById(R.id.tv_name);
		mName.setText(mAmbulances.get(position).text);

		BloodBankDao dao = new BloodBankDao(this, 1);
		BloodBank blood = dao.getBloodBank(mAmbulances.get(position).id);

		// Log.d("blood", blood.getAddress());

		TextView mAddress = (TextView) mDialog.findViewById(R.id.tv_address);
		mAddress.setText(blood.getAddress());

		final TextView mNumber = (TextView) mDialog
				.findViewById(R.id.tv_number);
		mNumber.setText(blood.getPhone());

		int colorIndex = (int) ((position + 1) % Constants.COLORS.length);
		mLinearLayout.setBackgroundColor(getResources().getColor(
				Constants.COLORS[colorIndex]));

		ImageButton mCallBt = (ImageButton) mDialog.findViewById(R.id.bt_call);

		mCallBt.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + mNumber.getText()));
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("Calling a Phone Number", "Call failed",
							activityException);
				}
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}

}
