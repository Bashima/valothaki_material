package com.cyan.valothaki.activity.user;

import java.util.List;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.SingleTextWithNumberAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.DoctorSpecialityModel;

/*
 * This activity extends the DistrictBaseActivity and overrides loadData and
 * onListItemClick method. This activity is called from Dashboard activity by
 * onDoctorClick metod.
 */
public class DoctorSpecialityActivity extends DistrictBaseActivity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.DistrictBaseActivity#loadData() This
	 * method loads data from database District and Doctor table.
	 */
	List<SingleItem> mSpeciality;

	@Override
	public void loadData() {
		DoctorSpecialityModel.loadDataModel(mContext);
		int numberOfTotalSpeciality = DoctorSpecialityModel.items.size();
		mSpeciality = DoctorSpecialityModel.items.subList(0,
				numberOfTotalSpeciality);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);

		SingleTextWithNumberAdapter singleAdapter = new SingleTextWithNumberAdapter(
				this, R.layout.row_single_text_left_rectangle_right_circle,
				mSpeciality);
		setListAdapter(singleAdapter);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.DistrictBaseActivity#onListItemClick(android
	 * .widget.ListView, android.view.View, int, long) This method is action
	 * handler for on list item click which starts DoctorActivity.
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent mIntent;
		mIntent = new Intent(DoctorSpecialityActivity.this,
				DoctorActivity.class);
		// mIntent.putExtra("DistrictId", 1);
		SingleItem tempDoc = mSpeciality.get(position);
		int id1 = tempDoc.id;
		mIntent.putExtra("SpectId", id1);
		startActivity(mIntent);
	}

}
