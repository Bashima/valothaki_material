package com.cyan.valothaki.activity.user;

import java.util.List;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.SingleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.DistrictAmbulanceModel;

/*
 * This activity extends the DistrictBaseActivity and overrides loadData and
 * onListItemClick method. This activity is called from Dashboard activity by
 * onAmbulanceClick metod.
 */
public class DistrictAmbulanceActivity extends DistrictBaseActivity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyan.valothaki.activity.DistrictBaseActivity#loadData() This
	 * method loads data from database District and Ambulance table.
	 */
	public void loadData() {
		DistrictAmbulanceModel.loadDataModel(mContext);
		int numberOfTotalHospital = DistrictAmbulanceModel.items.size();
		List<SingleItem> clients = DistrictAmbulanceModel.items.subList(0,
				numberOfTotalHospital);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);
		SingleTextAdapter singleAdapter = new SingleTextAdapter(this,
				R.layout.row_single_text_left_circle, clients);
		setListAdapter(singleAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyan.valothaki.activity.DistrictBaseActivity#onListItemClick(android
	 * .widget.ListView, android.view.View, int, long) This method is action
	 * handler for on list item click which starts AmbulanceActivity.
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent i;
		i = new Intent(DistrictAmbulanceActivity.this, AmbulanceActivity.class);
		i.putExtra("district", String.valueOf(id+1));
		startActivity(i);
	}
}
