package com.cyan.valothaki.activity.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cyan.valothaki.R;
import com.cyan.valothaki.dao.DoctorDao;
import com.cyan.valothaki.dao.SpecialityDao;
import com.cyan.valothaki.entity.Chamber;
import com.cyan.valothaki.entity.Doctor;
import com.cyan.valothaki.entity.Specialization;
import com.cyan.valothaki.webservice.UserAddAppointmentWebService;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

public class AppointmentAddActivity extends FragmentActivity implements
		OnDateSetListener, TimePickerDialog.OnTimeSetListener {

	public static final String DATEPICKER_TAG = "datepicker";
	private Spinner mSpecialitySpinner;
	private Spinner mDoctorSpinner;
	private Spinner mChamberSpinner;
	private TextView mDateButton;
	List<Specialization> specs;
	List<Chamber> chambers;
	int specialId, chamberId;
	int docId = 0;
	List<Doctor> docs;
	DoctorDao docDao;
	public Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_appointment_add);
		docDao = new DoctorDao(this);
		mDateButton = (TextView) findViewById(R.id.tv_date);

		mContext= this;
		SpecialityDao specdao = new SpecialityDao(this);
		// List<String> mSpinnerArraySpeciality = new ArrayList<String>();
		// mSpinnerArraySpeciality.add("Choose a speciality");
		// mSpinnerArraySpeciality.add("Cardiologist");
		// mSpinnerArraySpeciality.add("Neurologist");
		// mSpinnerArraySpeciality.add("Cardiologist");
		specs = specdao.PopulateSpec();
		List<String> mSpinnerArraySpeciality = specdao.Populate();
		ArrayAdapter<String> mAdapterSpeciality = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item,
				mSpinnerArraySpeciality);
		mAdapterSpeciality
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpecialitySpinner = (Spinner) findViewById(R.id.sp_speciality);
		mSpecialitySpinner.setAdapter(mAdapterSpeciality);
		mSpecialitySpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						specSpinnerHandler();

					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		// mSpecialitySpinner.oni
		// String
		// specIdStr=String.valueOf(mSpecialitySpinner.getSelectedItem());
		// int specialId = 0;
		// for(int i=0;i<specs.size();i++)
		// {
		// Specialization spect=specs.get(i);
		// if(spect.getName().equals(specIdStr))
		// {
		// specialId=spect.getSpecializationId();
		//
		// }
		// }

		// List<String> mSpinnerArrayDoctor = new ArrayList<String>();
		// mSpinnerArrayDoctor.add("Choose a doctor");
		// mSpinnerArrayDoctor.add("Naznin Nahar");
		// mSpinnerArrayDoctor.add("Sumaya Tasneem");
		// mSpinnerArrayDoctor.add("Tanzin Tabassum");

		// List<String> mSpinnerArrayChamber = new ArrayList<String>();
		// mSpinnerArrayChamber.add("Choose a chamber");
		// mSpinnerArrayChamber.add("Podda");
		// mSpinnerArrayChamber.add("Mukto");
		// mSpinnerArrayChamber.add("Popular");

		// String selected = sItems.getSelectedItem().toString();
		// if (selected.equals("what ever the option was")) {
		// }

		final Calendar calendar = Calendar.getInstance();

		Time now = new Time();
		now.setToNow();
		mDateButton.setText(now.year + "-" + now.month + "-" + now.monthDay);
		final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
				this, calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);

		mDateButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				datePickerDialog.setVibrate(false);
				datePickerDialog.setYearRange(1985, 2028);
				datePickerDialog.setCloseOnSingleTapDay(true);
				datePickerDialog.show(getSupportFragmentManager(),
						DATEPICKER_TAG);
			}
		});

		if (savedInstanceState != null) {
			DatePickerDialog dpd = (DatePickerDialog) getSupportFragmentManager()
					.findFragmentByTag(DATEPICKER_TAG);
			if (dpd != null) {
				dpd.setOnDateSetListener(this);
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.appointment_add, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_sync) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		mDateButton.setText(year + "-" + month + "-" + day);

	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub

	}

	public void onAddAppointmentAddClick(View v) {
		String chamberName = String.valueOf(mChamberSpinner.getSelectedItem());
		for (int i = 0; i < chambers.size(); i++) {
			Chamber temp = chambers.get(i);
			if (temp.getName().equals(chamberName)) {
				chamberId = temp.getId();
				break;
			}

		}
		String arg = docId + "$" + mDateButton.getText() + "$" + chamberId;

		new UserAddAppointmentWebService(arg).execute();
		Toast.makeText(mContext, "Your request has been sent.", Toast.LENGTH_LONG).show();
		finish();

	}

	public void specSpinnerHandler() {
		String specIdStr = String.valueOf(mSpecialitySpinner.getSelectedItem());
		specialId = 0;
		for (int i = 0; i < specs.size(); i++) {
			Specialization spect = specs.get(i);
			if (spect.getName().equals(specIdStr)) {
				specialId = spect.getSpecializationId();
				Log.d("spec: ", spect.getName());

			}
		}

		docs = docDao.PopulateSpec(specialId);
		List<String> mSpinnerArrayDoctor = new ArrayList<String>();
		for (int i = 0; i < docs.size(); i++)
			mSpinnerArrayDoctor.add(docs.get(i).getName());

		ArrayAdapter<String> mAdapterDoctor = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSpinnerArrayDoctor);
		mAdapterDoctor
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mDoctorSpinner = (Spinner) findViewById(R.id.sp_doctor);
		mDoctorSpinner.setAdapter(mAdapterDoctor);
		mDoctorSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				docSpinHandler();

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public void docSpinHandler() {
		String name = String.valueOf(mDoctorSpinner.getSelectedItem());
		for (int i = 0; i < docs.size(); i++) {
			Doctor temp = docs.get(i);
			if (temp.getName().equals(name)) {
				docId = temp.getUserId();
				break;
			}

		}
		chambers = docDao.getChamber(docId);

		List<String> mSpinnerArrayChamber = new ArrayList<String>();

		for (int i = 0; i < chambers.size(); i++)
			mSpinnerArrayChamber.add(chambers.get(i).getName());
		ArrayAdapter<String> mAdapterChamber = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mSpinnerArrayChamber);
		mAdapterChamber
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mChamberSpinner = (Spinner) findViewById(R.id.sp_chamber);
		mChamberSpinner.setAdapter(mAdapterChamber);

	}

}
