package com.cyan.valothaki.activity.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.util.Constants;

/*
 * This class extends Activity and implements AdapterView.OnItemClickListner.
 * This class is the abstract class which extended by several other classes.
 * This class is responsible for generating a list view with sticky  header 
 * along with search integration.
 */
public abstract class StickyListBaseActivity extends Activity implements
		AdapterView.OnItemClickListener {

	public int mId;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle) This class creates
	 * the layout and initiate loadData method.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_sticky_list);
		mId = getIntent().getIntExtra(Constants.ID, -1);
		dataLoad();
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	/*
	 * This abstract method will be override by the derived classes in order to
	 * load date from database.
	 */
	public abstract void dataLoad();

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu) Inflate
	 * the menu; this adds items to the action bar if it is present.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.sticky_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		// int id = item.getItemId();
		// if (id == R.id.action_settings) {
		// return true;
		// }
		// else if (id == R.id.action_help) {
		// Intent mIntent = new Intent(StickyListBaseActivity.this,
		// HelpActivity.class);
		// startActivity(mIntent);
		// return true;
		// }
		// return super.onOptionsItemSelected(item);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget
	 * .AdapterView, android.view.View, int, long) This abstract method is
	 * override by the derived class in order to enable on list item click
	 * action handler.
	 */
	public abstract void onItemClick(AdapterView<?> parent, View view,
			int position, long id);
}
