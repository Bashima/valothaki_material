package com.cyan.valothaki.activity.base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.cyan.valothaki.R;
import com.mikhaellopez.circularimageview.CircularImageView;

public class ProfileBaseActivity extends Activity {

	public TextView mName, mEmail, mPassword, mNumber, mAddress;
	public CircularImageView mProfile;
	private String picturePath = "";
	private String source="";
	
	private static int RESULT_LOAD_IMAGE = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		
		mProfile = (CircularImageView) findViewById(R.id.profile);
		mProfile.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent i = new Intent(
							Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							 
							startActivityForResult(i, RESULT_LOAD_IMAGE);
				} catch (ActivityNotFoundException activityException) {
					
				}
			}
		});
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile, menu);
		return true;
		
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            source = selectedImage.toString();
            		cursor.getString(columnIndex);
            cursor.close();
             
           
            savePicture();
            
         
        }
     
     
    }
	
	@Override
 	protected void onResume() {
 		super.onResume();
 
 		File file = new File(
 				this.getExternalFilesDir(Environment.DIRECTORY_DCIM),
 				"profilepic.png");
 		if (file.exists()) {
 			Drawable d = Drawable.createFromPath(file.getPath());
 			mProfile.setImageDrawable(d);
 		}
	}
	
	private void savePicture() {
		 mProfile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
 			File destination = new File(
 					this.getExternalFilesDir(Environment.DIRECTORY_DCIM),
 					"profilepic.png");
 			File sourcef = new File(source);
 			try {
 				FileUtils.copyFile(sourcef, destination);
 			} catch (IOException e) {
 				Toast.makeText(this, "Error loading data", Toast.LENGTH_LONG)
 						.show();
 				e.printStackTrace();
 		}
 	}
}
