package com.cyan.valothaki.activity.base;

import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.ForumQuestionAnswerActivity;
import com.cyan.valothaki.activity.user.HelpActivity;
import com.cyan.valothaki.adapter.ForumQuestionAdapter;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.ForumQuestionModel;
import com.cyan.valothaki.webservice.UserPostAnswerWebService;
import com.cyan.valothaki.webservice.UserPostQuestionWebService;
import com.cyan.valothaki.webservice.UserSyncQuestion;
import com.shamanland.fab.FloatingActionButton;
import com.shamanland.fab.ShowHideOnScroll;

public class ForumQuestionListBaseActivity extends DistrictBaseActivity {

	List<SingleItem> questions;
	public FloatingActionButton fab;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forum_question_list);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		fab = (FloatingActionButton) findViewById(R.id.fab);

	
		fab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onQuestionClick();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forum_question_list, menu);
		return true;
	}

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_sync) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			new UserSyncQuestion(mContext).execute();
			// Log.d("dist: ", String.valueOf(district));
			Intent mIntent = new Intent(ForumQuestionListBaseActivity.this,
					ForumQuestionListBaseActivity.class);
			startActivity(mIntent);
			return true;
		} else if (id == R.id.action_help) {
			Intent mIntent = new Intent(ForumQuestionListBaseActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void loadData() {
		// TODO Auto-generated method stub
		ForumQuestionModel.loadDataModel(mContext);
		int numberOfTotalQuestions = ForumQuestionModel.items.size();
		questions = ForumQuestionModel.items.subList(0, numberOfTotalQuestions);

		listView = (ListView) findViewById(android.R.id.list);
		
		Log.d("list ", listView.toString());
		//Log.d("fab ", fab.toString());
		//listView.setOnTouchListener(new ShowHideOnScroll(fab));
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);
		ForumQuestionAdapter singleAdapter = new ForumQuestionAdapter(this,
				R.layout.row_single_text_left_circle, questions);
		setListAdapter(singleAdapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent i;
		i = new Intent(ForumQuestionListBaseActivity.this,
				ForumQuestionAnswerActivity.class);
		i.putExtra("qId", questions.get(position).id);
		Log.d("q", String.valueOf(questions.get(position).id));
		ConstantsUser.qId = String.valueOf(questions.get(position).id);
		startActivity(i);
	}
	
	protected void onQuestionClick() {
		// TODO Auto-generated method stub
		final Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_question_forum);
		mDialog.setCancelable(true);
		final EditText quesBox=(EditText) mDialog.findViewById(R.id.et_answer);

		Button mDoneButton = (Button) mDialog.findViewById(R.id.bt_done);
		// if button is clicked, close the custom dialog
		mDoneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String quesP=quesBox.getText().toString();
				Log.d("ans: ",quesP);
   			String arg=quesP;
				new UserPostQuestionWebService(arg).execute();
				mDialog.dismiss();
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}
}
