package com.cyan.valothaki.activity.base;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.user.HelpActivity;
import com.cyan.valothaki.util.Constants;

/*
 * This is the DistrictBaseActivity which is an abstract class extending ListActivity.
 * This has 2 abstract class load data and onListItem click.
 *  This class is being extended by 4 more activities.
 */
public abstract class DistrictBaseActivity extends ListActivity {

	public ListView listView;
	public View lastLine;
	public String page;
	public Context mContext;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle) This method is
	 * responsible for setting layout and calling loadData method.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_district);
		page = getIntent().getStringExtra(Constants.PAGE);
		lastLine = findViewById(R.id.last_border);
		mContext= this;
		loadData();
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	/*
	 * This is an abstract class which will be overridden.
	 */
	public abstract void loadData();

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hospital, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 * This is called when any action menu is being pressed.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		} else if (id == R.id.action_help) {
			Intent mIntent = new Intent(DistrictBaseActivity.this,
					HelpActivity.class);
			startActivity(mIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView,
	 * android.view.View, int, long) This abstract method should be override by
	 * the classes which extend this class in order to enable handler of click
	 * on an item of the list.
	 */
	@Override
	public abstract void onListItemClick(ListView l, View v, int position,
			long id);
}
