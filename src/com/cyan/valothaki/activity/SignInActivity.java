package com.cyan.valothaki.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cyan.valothaki.R;
import com.cyan.valothaki.webservice.SignInWebService;

public class SignInActivity extends Activity {

	public EditText mEmailEt, mPassWordEt;
	public Button mSignInBt;
	public TextView mRegisterTv;
	public String mEmail, mPassword;
	public Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);

		mContext = this;
		mEmailEt = (EditText) findViewById(R.id.et_email);
		mPassWordEt = (EditText) findViewById(R.id.et_password);
		mSignInBt = (Button) findViewById(R.id.bt_sign_in);
		mRegisterTv = (TextView) findViewById(R.id.tv_sign_up);
		
		mSignInBt.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mEmail = mEmailEt.getText().toString();
				mPassword = mPassWordEt.getText().toString();
				if(!mEmail.equals("") && (!mPassword.equals("")))
				{
					new SignInWebService(mContext,mEmail, mPassword).execute();
					
					
//					Intent mIntent = new Intent(SignInActivity.this,
//							DashboardActivity.class);
//					startActivity(mIntent);
				}
				else
				{
					Toast.makeText(mContext, "Please enter email & password", Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_in, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
