package com.cyan.valothaki.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cyan.valothaki.R;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dashboard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onLogInClick(View v) {
		Intent i = new Intent(MainActivity.this, SignInActivity.class);
		startActivity(i);
	}
	public void onSignUpClick(View v)
	{
		Intent i= new Intent(MainActivity.this,SignUpUserActivity.class);
		startActivity(i);
	}
}
