package com.cyan.valothaki.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.ForumQuestionListBaseActivity;
import com.cyan.valothaki.adapter.AnswerTextAdapter;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.dao.QuestionDao;
import com.cyan.valothaki.entity.Answer;
import com.cyan.valothaki.entity.Question;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.DistrictAmbulanceModel;
import com.cyan.valothaki.webservice.UserAddAppointmentWebService;
import com.cyan.valothaki.webservice.UserPostAnswerWebService;
import com.cyan.valothaki.webservice.UserSyncAnswer;
import com.shamanland.fab.FloatingActionButton;
import com.shamanland.fab.ShowHideOnScroll;

public class ForumQuestionAnswerActivity extends Activity implements
		AdapterView.OnItemClickListener {

	public TextView mQuestion;
	public TextView mPoster;
	public TextView mDateTime;
	ListView expListView;
	public Context mContext;
	public int questionId;
	QuestionDao qDao;
	
	Question question;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forum_question_answer);
		
		Intent intent = getIntent();
		if (null != intent) {
			// district=intent.getIntExtra("district", 0);
			String d = intent.getStringExtra("qId");
			this.questionId = Integer.parseInt(ConstantsUser.qId);
			Log.d("district is: ", String.valueOf(this.questionId));

		}
		
		
		mContext = this;
		mQuestion = (TextView) findViewById(R.id.tv_question);
		mPoster = (TextView) findViewById(R.id.tv_poster);
		mDateTime = (TextView) findViewById(R.id.tv_date_time);
		
		qDao=new QuestionDao(mContext);
		question=qDao.getQuestion(this.questionId);
		

		mQuestion.setText(question.getDescription());
		mPoster.setText(question.getUserName());
		mDateTime.setText(question.getDate()+"\n"+question.getTime());

//		DistrictAmbulanceModel.loadDataModel(mContext);
//		int numberOfTotalHospital = DistrictAmbulanceModel.items.size();
//		List<SingleItem> clients = DistrictAmbulanceModel.items.subList(0,
//				numberOfTotalHospital);
		List<SingleItem> clients=new ArrayList<SingleItem>();
		List<Answer> ans=qDao.getAnswer(questionId);
		for(int i=0;i<ans.size();i++)
		{
			clients.add(new SingleItem(ans.get(i).getAnswerId(),ans.get(i).getDescription()));
			
		}

		// get the listview
		expListView = (ListView) findViewById(R.id.cardlist);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

		expListView.setOnTouchListener(new ShowHideOnScroll(fab));

		fab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onAnswerClick();
			}
		});

		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		expListView.startAnimation(animation);
		expListView.setOnItemClickListener(this);
		// //
		AnswerTextAdapter listAdapter = new AnswerTextAdapter(this,
				R.layout.row_answer, ans,clients);

		// setting list adapter
		expListView.setAdapter(listAdapter);

		Log.d("si", String.valueOf(clients.size()));
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	protected void onAnswerClick() {
		// TODO Auto-generated method stub
		final Dialog mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.pop_up_answer_forum);
		mDialog.setCancelable(true);
		final EditText ansBox=(EditText) mDialog.findViewById(R.id.et_answer);

		Button mDoneButton = (Button) mDialog.findViewById(R.id.bt_done);
		// if button is clicked, close the custom dialog
		mDoneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String ansP=ansBox.getText().toString();
				Log.d("ans: ",ansP);
				String arg=ansP+"\\$"+questionId;
				new UserPostAnswerWebService(arg).execute();
				mDialog.dismiss();
			}
		});
		mDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forum_question_answer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			new UserSyncAnswer(mContext, questionId).execute();
			Intent i;
			i = new Intent(ForumQuestionAnswerActivity.this,
					ForumQuestionAnswerActivity.class);
			i.putExtra("qId", questionId);
			startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}
}
