package com.cyan.valothaki.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.cyan.valothaki.R;
import com.cyan.valothaki.webservice.SignUpWebService;

public class SignUpUserActivity extends Activity {
	private EditText firstName, lastName, phone, email, password, city, road;
	public RadioButton male, female;
	EditText district;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up_user);
		firstName = (EditText) findViewById(R.id.et_FirstName);
		lastName = (EditText) findViewById(R.id.et_LastName);
		phone = (EditText) findViewById(R.id.et_phone);
		email = (EditText) findViewById(R.id.et_email);
		password = (EditText) findViewById(R.id.et_password);
		city = (EditText) findViewById(R.id.et_city);
		road = (EditText) findViewById(R.id.et_house_road);
		male = (RadioButton) findViewById(R.id.rb_male);
		female = (RadioButton) findViewById(R.id.rb_female);
		district = (EditText) findViewById(R.id.sp_district);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onSignUpClick(View v) {
		// Intent i = new Intent(MainActivity.this, DashboardActivity.class);
		// startActivity(i);

		String arg = firstName.getText() + "$" + lastName.getText() + "$"
				+ "12" + "$" + road.getText() + "$" + city.getText() + "$"
				+ district.getText() + "$" + phone.getText() + "$" + email.getText() + "$"
				+ "male" + "$" + "active" + "$" + password.getText();
		new SignUpWebService(arg).execute();
		Intent i = new Intent(SignUpUserActivity.this, MainActivity.class);
		startActivity(i);
	}
}
