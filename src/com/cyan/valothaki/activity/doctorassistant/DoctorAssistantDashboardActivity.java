package com.cyan.valothaki.activity.doctorassistant;

import java.io.File;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.cyan.valothaki.R;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.webservice.DoctorPendingRequest;
import com.mikhaellopez.circularimageview.CircularImageView;

public class DoctorAssistantDashboardActivity extends Activity {

	public CircularImageView mProfile;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor_assistant_dashboard);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mProfile=(CircularImageView) findViewById(R.id.profile);
		File file = new File(
 				this.getExternalFilesDir(Environment.DIRECTORY_DCIM),
 				"profilepic.png");
 		if (file.exists()) {
 			Drawable d = Drawable.createFromPath(file.getPath());
 			mProfile.setImageDrawable(d);
 		}
 		else
 		
		mProfile.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent mIntent = new Intent(DoctorAssistantDashboardActivity.this,
							DoctorAsstProfileActivity.class);
					startActivity(mIntent);
				} catch (ActivityNotFoundException activityException) {
					
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doctor_assistant_dashboard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onPendingClick(View v) {
//		Intent mIntent = new Intent(DoctorAssistantDashboardActivity.this,
//				AcceptingPatientsActivity.class);
//		startActivity(mIntent);
		new DoctorPendingRequest(getApplicationContext(),1).execute();
	}
	
	public void onDoctorClick(View v) {
		Intent mIntent = new Intent(DoctorAssistantDashboardActivity.this,
				DoctorContactActivity.class);
		startActivity(mIntent);
	}
	
	public void onForumClick(View v) {
		Intent mIntent = new Intent(DoctorAssistantDashboardActivity.this,
				DocAsstForumQuestionListActivity.class);
		startActivity(mIntent);
	}
	
	public void onAppointmentClick(View v) {
//		Intent mIntent = new Intent(DoctorAssistantDashboardActivity.this,
//				ShowPatientListActivity.class);
//		startActivity(mIntent);
		new DoctorPendingRequest(getApplicationContext(),2).execute();
	}
}
