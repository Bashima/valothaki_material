package com.cyan.valothaki.activity.doctorassistant;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.PatientListTextAdapter;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.item.DoubleItem;
import com.cyan.valothaki.model.ShowPatientModel;

public class ShowPatientListActivity extends DistrictBaseActivity {

	List<DoubleItem> clients;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_patient_list);
		//ekhane intent hishebe chamber id pass korio
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_patient_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void loadData() {
		// TODO Auto-generated method stub
		ShowPatientModel.loadDataModel(mContext);
		int numberOfTotalPatients = ShowPatientModel.items.size();
		clients = ShowPatientModel.items.subList(0,
				numberOfTotalPatients);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);
		PatientListTextAdapter doubleAdapter = new PatientListTextAdapter(this,
				R.layout.row_double_text_left_circle, clients);
		setListAdapter(doubleAdapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent i;
		i = new Intent(ShowPatientListActivity.this, GivingDateActivity.class);
//		i.putExtra("district", String.valueOf(id+1));
		i.putExtra("FROM", "EDIT");
		i.putExtra("id", String.valueOf(clients.get(position).id));
		ConstantsUser.apId=String.valueOf(clients.get(position).id);
		startActivity(i);
	}
}
