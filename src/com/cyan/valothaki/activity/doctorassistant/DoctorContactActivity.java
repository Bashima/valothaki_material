package com.cyan.valothaki.activity.doctorassistant;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cyan.valothaki.R;

public class DoctorContactActivity extends Activity {

	public TextView mName, mSpeciality, mNumber, mAddress;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor_contact);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mName = (TextView) findViewById(R.id.tv_name);
		mSpeciality = (TextView) findViewById(R.id.tv_speciality_doctor);
		mNumber = (TextView) findViewById(R.id.tv_number);
		mAddress = (TextView) findViewById(R.id.tv_address_doctor);
		
		mName.setText("Dr. Naznin Nahar");
		mSpeciality.setText("Neuromedicine");
		mNumber.setText("017140007479");
		mAddress.setText("3/1 Segun Bagicha, Dhaka- 1000");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doctor_contact, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
