package com.cyan.valothaki.activity.doctorassistant;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.adapter.SingleTextAdapter;
import com.cyan.valothaki.constants.ConstantsUser;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.PatientListForAcceptanceModel;

public class AcceptingPatientsActivity extends DistrictBaseActivity {

	List<SingleItem> clients ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accepting_patients);
	}
	
	@Override
	public void loadData() {
		// TODO Auto-generated method stub
		PatientListForAcceptanceModel.loadDataModel(mContext);
		int numberOfTotalPatients = PatientListForAcceptanceModel.items.size();
		clients = PatientListForAcceptanceModel.items.subList(0,
				numberOfTotalPatients);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);
		SingleTextAdapter singleAdapter = new SingleTextAdapter(this,
				R.layout.row_single_text_left_circle, clients);
		setListAdapter(singleAdapter);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent i;
		i = new Intent(AcceptingPatientsActivity.this, GivingDateActivity.class);
//		i.putExtra("district", String.valueOf(id+1));
		i.putExtra("FROM", "ADD");
		i.putExtra("name", clients.get(position).text);
	
		ConstantsUser.apId=String.valueOf(clients.get(position).id);
		startActivity(i);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.accepting_patients, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
