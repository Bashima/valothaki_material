package com.cyan.valothaki.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.base.DistrictBaseActivity;
import com.cyan.valothaki.activity.doctorassistant.ShowPatientListActivity;
import com.cyan.valothaki.adapter.SingleTextAdapter;
import com.cyan.valothaki.item.SingleItem;
import com.cyan.valothaki.model.ChamberModel;

public class ChamberListActivity  extends DistrictBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chamber_list);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chamber_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_sync) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void loadData() {
		// TODO Auto-generated method stub
		ChamberModel.loadDataModel(mContext);
		int numberOfTotalChambers = ChamberModel.items.size();
		List<SingleItem> clients = ChamberModel.items.subList(0,
				numberOfTotalChambers);

		listView = (ListView) findViewById(android.R.id.list);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom_slow);
		listView.startAnimation(animation);
		lastLine.startAnimation(animation);
		SingleTextAdapter singleAdapter = new SingleTextAdapter(this,
				R.layout.row_single_text_left_circle, clients);
		setListAdapter(singleAdapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent i;
		i = new Intent(ChamberListActivity.this, ShowPatientListActivity.class);
//		i.putExtra("CHAMBERID", String.valueOf(id+1));
		startActivity(i);
	}
}
