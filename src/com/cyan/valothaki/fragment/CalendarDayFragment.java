package com.cyan.valothaki.fragment;

import hirondelle.date4j.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.user.AppointmentActivity;
import com.cyan.valothaki.util.Constants;
//import com.cyan.valothaki.activity.AppointmentActivity;
//import com.cyan.valothaki.utils.Constants;

@SuppressLint("SimpleDateFormat")
public class CalendarDayFragment extends Fragment {

	// private DatabaseHelper databaseHelper;
	private TextView tvMonth;
	public RelativeLayout dayContainers;
	// private RelativeLayout linearlayout;
	private ImageButton btLeft, btRight;
	// private Event mEvent;
	// private List<Event> events;
	 public DateTime mDay;

	private ScrollView sv;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_calendar_day, container,
				false);

		tvMonth = (TextView) view.findViewById(R.id.tv_month_name_head);

		dayContainers = (RelativeLayout) view.findViewById(R.id.rl_day);

		btLeft = (ImageButton) view.findViewById(R.id.bt_cal_left);
		btRight = (ImageButton) view.findViewById(R.id.bt_cal_right);
		sv = (ScrollView) view.findViewById(R.id.scrollView1);
		sv.post(new Runnable() {
			public void run() {
				float scale = getActivity().getResources().getDisplayMetrics().density;
				DateTime now = DateTime.now(TimeZone.getDefault());
				int nowTime = (int) (((now.getHour() * 60) + now.getMinute())
						* scale + 0.5f);
				sv.scrollTo(0, nowTime);
			}
		});
		setHasOptionsMenu(true);

		btLeft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppointmentActivity.viewI = AppointmentActivity.viewI - 1;
				AppointmentActivity.viewT = -1;
				refresh();
			}
		});

		btRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppointmentActivity.viewI = AppointmentActivity.viewI + 1;
				AppointmentActivity.viewT = 1;
				refresh();
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		setDate(AppointmentActivity.viewI, AppointmentActivity.viewT);
		// setEvent();
		super.onResume();
	}

	private void setDate(int i, int t) {
		DateTime d = DateTime.today(TimeZone.getDefault());
		DateTime day;
		if (t < 0) {
			if (i < 0) {
				day = d.minusDays(Math.abs(i));
			} else {
				day = d.plusDays(i);
			}
		} else if (t > 0) {
			day = d.plusDays(i);
		} else {
			day = d;
		}
		mDay = day;
		String weekDay = Constants.WEEK_DAYS[day.getWeekDay() - 1];

		tvMonth.setText(getDate(day.getMilliseconds(TimeZone.getDefault()))
				+ " " + day.getDay() + ", " + day.getYear() + " : " + weekDay);
	}

	public static String getDate(long milliSeconds) {
		DateFormat formatter = new SimpleDateFormat("MMM");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// switch (item.getItemId()) {
		// // case R.id.action_add:
		// // Intent i = new Intent(getActivity(), AddEventActivity.class);
		// // i.putExtra(Constants.TYPE, Constants.ADD_NEW);
		// // i.putExtra(Constants.SOURCE, Constants.DAY);
		// // i.putExtra(Constants.WORK, AppointmentActivity.source);
		// // if (AppointmentActivity.source.equals(Constants.CLIENT)) {
		// // i.putExtra(Constants.CLIENT_ID, AppointmentActivity.clientId);
		// // }
		// startActivity(i);
		return true;
		// default:
		// return super.onOptionsItemSelected(item);
		// }
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.add_menu, menu);
	// }

	// public void setEvent() {
	// final float scale = getActivity().getResources()
	// .getDisplayMetrics().density;
	// try {
	// Dao<Event, Long> dao = getHelper().getDao(Event.class);
	// QueryBuilder<Event, Long> builder = dao.queryBuilder();
	// builder.where().eq("date", mDay);
	// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
	// Dao<Case, Long> caseDao = getHelper().getDao(Case.class);
	// QueryBuilder<Case, Long> caseBuilder = caseDao.queryBuilder();
	// caseBuilder.where().eq("clientId", AppointmentActivity.clientId);
	// Log.d("client id ...........",
	// String.valueOf(AppointmentActivity.clientId));
	// List<Case> cases = caseDao.query(caseBuilder.prepare());
	// List<Long> ids = new ArrayList<Long>();
	// Log.d("id ...........", ids.toString());
	// for (Case caseItem : cases) {
	// ids.add(caseItem.getId());
	// Log.d("case id ...........",
	// String.valueOf(caseItem.getId()));
	// }
	// builder.where().in("caseId", ids);
	// }
	//
	// events = dao.query(builder.prepare());
	//
	// Log.d("All events", events.toString());
	//
	// // Sorting
	// Collections.sort(events, new Comparator<Event>() {
	// @Override
	// public int compare(Event event1, Event event2) {
	//
	// return event1.getStartTime().compareTo(
	// event2.getStartTime());
	// }
	// });
	//
	// for (int i = 0; i < events.size(); i++) {
	// Event event = events.get(i);
	// DateTime startTime = new DateTime(event.getDate() + " "
	// + event.getStartTime());
	// DateTime endTime = new DateTime(event.getDate() + " "
	// + event.getEndTime());
	//
	//
	// int mHeight = minuteConverter(endTime)
	// - minuteConverter(startTime);
	// int mHeightPixel = (int) (mHeight * scale + 0.5f);
	// Button myButton = new Button(getActivity());
	// myButton.setText(event.getEventName());
	// LayoutParams buttonparam = new LayoutParams(
	// LayoutParams.MATCH_PARENT, mHeightPixel);
	// int mTop = minuteConverter(startTime);
	// int mTopPixel = (int) (mTop * scale + 0.5f);
	// buttonparam.setMargins(1, mTopPixel, 1, 0);
	// myButton.setLayoutParams(buttonparam);
	// myButton.setTag(i);
	// myButton.setTextSize(12);
	//
	// int colorIndex = (int) (event.getCaseId() % Constants.COLORS.length);
	// myButton.setBackgroundColor(getResources().getColor(
	// Constants.COLORS[colorIndex]));
	//
	// dayContainers.addView(myButton);
	//
	// final Dialog dialog = new Dialog(getActivity());
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.popup_event_show);
	// myButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// linearlayout = (RelativeLayout) dialog
	// .findViewById(R.id.color_layout);
	// mEvent = events.get((Integer) v.getTag());
	// TextView mEventName = (TextView) dialog
	// .findViewById(R.id.et_show_event_name);
	// mEventName.setText(mEvent.getEventName());
	//
	// TextView mDate = (TextView) dialog
	// .findViewById(R.id.et_show_date);
	// mDate.setText(mEvent.getDate() + ": "
	// + mEvent.getStartTime() + " "
	// + mEvent.getEndTime());
	//
	// TextView mCaseName = (TextView) dialog
	// .findViewById(R.id.et_show_case_name);
	// mCaseName.setText(getCaseName(mEvent.getCaseId()));
	//
	// TextView mDescription = (TextView) dialog
	// .findViewById(R.id.et_show_desccription);
	// mDescription.setText(mEvent.getDescription());
	//
	// int colorIndex = (int) (mEvent.getCaseId() % Constants.COLORS.length);
	// linearlayout.setBackgroundColor(getResources()
	// .getColor(Constants.COLORS[colorIndex]));
	//
	// dialog.show();
	// AppointmentActivity.popup=true;
	//
	// }
	// });
	// Button editButton = (Button) dialog
	// .findViewById(R.id.bt_show_edit);
	// // if button is clicked, close the custom dialog
	// editButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// Intent i = new Intent(getActivity(),
	// AddEventActivity.class);
	// i.putExtra(Constants.TYPE, Constants.EDIT);
	// i.putExtra(Constants.ID, mEvent.getId());
	// i.putExtra(Constants.SOURCE, Constants.DAY);
	// i.putExtra(Constants.WORK, AppointmentActivity.source);
	// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
	// i.putExtra(Constants.CLIENT_ID,
	// AppointmentActivity.clientId);
	// }
	// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	// startActivity(i);
	// dialog.dismiss();
	// AppointmentActivity.popup=false;
	// }
	// });
	//
	// Button deleteButton = (Button) dialog
	// .findViewById(R.id.bt_show_delete);
	// // if button is clicked, close the custom dialog
	// deleteButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// delete(mEvent.getId());
	// refresh();
	// dialog.dismiss();
	// AppointmentActivity.popup=false;
	// }
	// });
	//
	// }
	// DateTime now = DateTime.now(TimeZone.getDefault());
	// int nowTime= (int) (( (now.getHour()*60)+now.getMinute())* scale + 0.5f);
	// Button nowView= new Button(getActivity());
	// nowView.setBackgroundColor(getResources().getColor(R.color.Red));
	// LayoutParams viewparam = new LayoutParams(
	// LayoutParams.MATCH_PARENT, 2);
	// viewparam.setMargins(1, nowTime, 1, 0);
	// nowView.setLayoutParams(viewparam);
	//
	// dayContainers.addView(nowView);
	// } catch (Exception e) {
	// e.printStackTrace();
	// Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG)
	// .show();
	// }
	//
	// }
	//
	//
	//
	// private DatabaseHelper getHelper() throws SQLException {
	// if (databaseHelper == null) {
	// databaseHelper = OpenHelperManager.getHelper(getActivity(),
	// DatabaseHelper.class);
	// }
	// return databaseHelper;
	// }

	public int minuteConverter(DateTime date) {
		int minute = date.getHour() * 60 + date.getMinute();
		return minute;
	}

	// public String getCaseName(long caseId) {
	// String caseName = "unable to load";
	// Case cases;
	// Dao<Case, Long> caseDao;
	// try {
	// caseDao = getHelper().getDao(Case.class);
	// cases = caseDao.queryForId(caseId);
	// caseName = cases.getName();
	// } catch (Exception e) {
	// Toast.makeText(getActivity(), "Error while loading case name",
	// Toast.LENGTH_LONG).show();
	// }
	//
	// return caseName;
	// }
	//
	// private void delete(long id) {
	// Dao<Event, Long> eventDao;
	// try {
	// eventDao = getHelper().getDao(Event.class);
	// Event event= eventDao.queryForId(id);
	// eventDao.deleteById(id);
	//
	// DeleteBuilder<Event, Long> eventDelete = eventDao.deleteBuilder();
	// eventDelete.where().eq("id", id);
	// eventDelete.delete();
	// if(event.isBillable()==true)
	// {
	// Intent intent = new Intent(getActivity(), BillReciever.class);
	// PendingIntent pendingIntent =
	// PendingIntent.getBroadcast(getActivity().getApplicationContext(), (int)
	// id, intent, 0);
	// getActivity().getBaseContext();
	// AlarmManager alarmManager = (AlarmManager)
	// getActivity().getSystemService(Context.ALARM_SERVICE);
	// alarmManager.cancel(pendingIntent);
	// }
	// if(event.isReminder()==true){
	// Intent intent = new Intent(getActivity(), ReminderReciever.class);
	// PendingIntent pendingIntent =
	// PendingIntent.getBroadcast(getActivity().getApplicationContext(), (int)
	// id, intent, 0);
	// getActivity().getBaseContext();
	// AlarmManager alarmManager = (AlarmManager)
	// getActivity().getSystemService(Context.ALARM_SERVICE);
	// alarmManager.cancel(pendingIntent);
	// }
	// Toast.makeText(getActivity(), "Event Deleted", Toast.LENGTH_LONG);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

	public void refresh() {
		FragmentManager manager = getActivity().getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		Fragment newFragment = this;
		this.onDestroy();
		ft.remove(this);
		ft.replace(this.getId(), newFragment);
		// container is the ViewGroup of current fragment
		ft.addToBackStack(null);
		ft.commit();
	}

}