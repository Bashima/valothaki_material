package com.cyan.valothaki.fragment;

import hirondelle.date4j.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.user.AppointmentActivity;

@SuppressLint({ "SimpleDateFormat", "NewApi", "ShowToast" })
public class CalendarWeekFragment extends Fragment {

	// private DatabaseHelper databaseHelper;
	private TextView tvSun;
	private TextView tvMon;
	private TextView tvTue;
	private TextView tvWed;
	private TextView tvThu;
	private TextView tvFri;
	private TextView tvSat;
	private TextView tvMonth;
	private DateTime mSun, mMon, mTue, mWed, mThu, mFri, mSat;
	private RelativeLayout[] dayContainers = new RelativeLayout[7];
	public RelativeLayout linearlayout;
	public RelativeLayout linearlayout_full;
	private ImageButton btLeft, btRight;
	private ScrollView sv;

	// private Event mEvent;
	// private List<Event> events;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_calendar_week,
				container, false);

		tvSun = (TextView) view.findViewById(R.id.tv_sun);
		tvMon = (TextView) view.findViewById(R.id.tv_mon);
		tvTue = (TextView) view.findViewById(R.id.tv_tue);
		tvWed = (TextView) view.findViewById(R.id.tv_wed);
		tvThu = (TextView) view.findViewById(R.id.tv_thu);
		tvFri = (TextView) view.findViewById(R.id.tv_fri);
		tvSat = (TextView) view.findViewById(R.id.tv_sat);
		tvMonth = (TextView) view.findViewById(R.id.tv_month_name_head);
		sv = (ScrollView) view.findViewById(R.id.scrollView1);

		sv.post(new Runnable() {
			public void run() {
				float scale = getActivity().getResources().getDisplayMetrics().density;
				DateTime now = DateTime.now(TimeZone.getDefault());
				int nowTime = (int) (((now.getHour() * 60) + now.getMinute())
						* scale + 0.5f);
				sv.scrollTo(0, nowTime);
			}
		});

		linearlayout_full = (RelativeLayout) view
				.findViewById(R.id.relativeLayout3);
		dayContainers[0] = (RelativeLayout) view.findViewById(R.id.day1);
		dayContainers[1] = (RelativeLayout) view.findViewById(R.id.day2);
		dayContainers[2] = (RelativeLayout) view.findViewById(R.id.day3);
		dayContainers[3] = (RelativeLayout) view.findViewById(R.id.day4);
		dayContainers[4] = (RelativeLayout) view.findViewById(R.id.day5);
		dayContainers[5] = (RelativeLayout) view.findViewById(R.id.day6);
		dayContainers[6] = (RelativeLayout) view.findViewById(R.id.day7);

		btLeft = (ImageButton) view.findViewById(R.id.bt_cal_left);
		btRight = (ImageButton) view.findViewById(R.id.bt_cal_right);
		setHasOptionsMenu(true);

		btLeft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppointmentActivity.viewI = AppointmentActivity.viewI - 7;
				AppointmentActivity.viewT = -1;
				refresh();
			}
		});

		btRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppointmentActivity.viewI = AppointmentActivity.viewI + 7;
				AppointmentActivity.viewT = 1;
				refresh();
			}
		});

		return view;
	}

	@Override
	public void onResume() {
		setDate(AppointmentActivity.viewI, AppointmentActivity.viewT);
		// setEvent();
		super.onResume();
	}

	private DateTime firstDayOfThisWeek() {
		DateTime today = DateTime.today(TimeZone.getDefault());
		DateTime firstDayThisWeek = today; // start value
		int todaysWeekday = today.getWeekDay();
		int SUNDAY = 1;
		if (todaysWeekday > SUNDAY) {
			int numDaysFromSunday = todaysWeekday - SUNDAY;
			firstDayThisWeek = today.minusDays(numDaysFromSunday);
		}
		return firstDayThisWeek;
	}

	private void setDate(int i, int t) {
		DateTime d = firstDayOfThisWeek();
		DateTime day;
		if (t < 0) {
			if (i < 0) {
				day = d.minusDays(Math.abs(i));
			} else {
				day = d.plusDays(i);
			}
		} else if (t > 0) {
			day = d.plusDays(i);
		} else {
			day = d;
		}
		mSun = day;
		mMon = day.plusDays(1);
		mTue = day.plusDays(2);
		mWed = day.plusDays(3);
		mThu = day.plusDays(4);
		mFri = day.plusDays(5);
		mSat = day.plusDays(6);

		tvSun.setText("SUN " + mSun.getDay());
		tvMon.setText("MON " + mMon.getDay());
		tvTue.setText("TUE " + mTue.getDay());
		tvWed.setText("WED " + mWed.getDay());
		tvThu.setText("THU " + mThu.getDay());
		tvFri.setText("FRI " + mFri.getDay());
		tvSat.setText("SAT " + mSat.getDay());

		if (day.getMonth().equals(day.plusDays(6).getMonth())) {
			tvMonth.setText(getDate(day.getMilliseconds(TimeZone.getDefault()))
					+ " " + day.getYear());
		} else {
			tvMonth.setText(getDate(day.getMilliseconds(TimeZone.getDefault()))
					+ " "
					+ day.getYear()
					+ " - "
					+ getDate(day.plusDays(6).getMilliseconds(
							TimeZone.getDefault())) + " "
					+ day.plusDays(6).getYear());
		}

		if (DateTime.now(TimeZone.getDefault()).gteq(mSun)
				&& DateTime.now(TimeZone.getDefault()).lteq(mSat)) {
			switch (DateTime.now(TimeZone.getDefault()).getWeekDay()) {
			case 1:
				tvSun.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 2:
				tvMon.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 3:
				tvTue.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 4:
				tvWed.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 5:
				tvThu.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 6:
				tvFri.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			case 7:
				tvSat.setTextColor(getResources().getColor(R.color.Blue700));
				break;
			default:
				break;
			}
		} else {
			tvSun.setTextColor(getResources().getColor(R.color.Grey600));
			tvMon.setTextColor(getResources().getColor(R.color.Grey600));
			tvTue.setTextColor(getResources().getColor(R.color.Grey600));
			tvWed.setTextColor(getResources().getColor(R.color.Grey600));
			tvThu.setTextColor(getResources().getColor(R.color.Grey600));
			tvFri.setTextColor(getResources().getColor(R.color.Grey600));
			tvSat.setTextColor(getResources().getColor(R.color.Grey600));
		}
	}

	public static String getDate(long milliSeconds) {
		DateFormat formatter = new SimpleDateFormat("MMM");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// switch (item.getItemId()) {
		// case R.id.action_add:
		// Intent i = new Intent(getActivity(), AddEventActivity.class);
		// i.putExtra(Constants.TYPE, Constants.ADD_NEW);
		// i.putExtra(Constants.SOURCE, Constants.WEEK);
		// i.putExtra(Constants.WORK, AppointmentActivity.source);
		// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
		// i.putExtra(Constants.CLIENT_ID, AppointmentActivity.clientId);
		// }
		// startActivity(i);
		return true;
		// default:
		// return super.onOptionsItemSelected(item);
		// }
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.add_menu, menu);
	}

	// public void setEvent() {
	// try {
	// final float scale = getActivity().getResources()
	// .getDisplayMetrics().density;
	//
	// Dao<Event, Long> dao = getHelper().getDao(Event.class);
	// QueryBuilder<Event, Long> builder = dao.queryBuilder();
	// builder.where().ge("date", mSun).and().lt("date", mSat.plusDays(1));
	// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
	// Log.d("source ...........", AppointmentActivity.source);
	//
	// Dao<Case, Long> caseDao = getHelper().getDao(Case.class);
	// QueryBuilder<Case, Long> caseBuilder = caseDao.queryBuilder();
	// caseBuilder.where().eq("clientId", AppointmentActivity.clientId);
	// Log.d("client id ...........",
	// String.valueOf(AppointmentActivity.clientId));
	// List<Case> cases = caseDao.query(caseBuilder.prepare());
	// List<Long> ids = new ArrayList<Long>();
	// for (Case caseItem : cases) {
	// ids.add(caseItem.getId());
	// Log.d("case id ...........",
	// String.valueOf(caseItem.getId()));
	// }
	// builder.where().in("caseId", ids);
	// }
	// events = dao.query(builder.prepare());
	// Log.d("events id ...........", events.toString());
	// Log.d("All events", events.toString());
	//
	// // Sorting
	// Collections.sort(events, new Comparator<Event>() {
	// @Override
	// public int compare(Event event1, Event event2) {
	//
	// return event1.getStartTime().compareTo(
	// event2.getStartTime());
	// }
	// });
	//
	// for (int i = 0; i < events.size(); i++) {
	// Event event = events.get(i);
	// DateTime startTime = new DateTime(event.getDate() + " "
	// + event.getStartTime());
	// DateTime endTime = new DateTime(event.getDate() + " "
	// + event.getEndTime());
	// Integer day = startTime.getWeekDay();
	//
	// int mHeight = minuteConverter(endTime)
	// - minuteConverter(startTime);
	// int mHeightPixel = (int) (mHeight * scale + 0.5f);
	// Button myButton = new Button(getActivity());
	// myButton.setText(event.getEventName());
	// LayoutParams buttonparam = new LayoutParams(
	// LayoutParams.MATCH_PARENT, mHeightPixel);
	// int mTop = minuteConverter(startTime);
	// int mTopPixel = (int) (mTop * scale + 0.5f);
	// buttonparam.setMargins(2, mTopPixel, 2, 0);
	// myButton.setLayoutParams(buttonparam);
	// myButton.setTag(i);
	// myButton.setTextSize(10);
	// myButton.setGravity(Gravity.LEFT | Gravity.TOP);
	//
	// int colorIndex = (int) (event.getCaseId() % Constants.COLORS.length);
	// myButton.setBackgroundColor(getResources().getColor(
	// Constants.COLORS[colorIndex]));
	//
	// dayContainers[day - 1].setBackground(getResources()
	// .getDrawable (R.drawable.calendar_border_ash));
	// dayContainers[day - 1].addView(myButton);
	//
	// final Dialog dialog = new Dialog(getActivity());
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.popup_event_show);
	// myButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// linearlayout = (RelativeLayout) dialog
	// .findViewById(R.id.color_layout);
	// mEvent = events.get((Integer) v.getTag());
	// TextView mEventName = (TextView) dialog
	// .findViewById(R.id.et_show_event_name);
	// mEventName.setText(mEvent.getEventName());
	//
	// TextView mDate = (TextView) dialog
	// .findViewById(R.id.et_show_date);
	// mDate.setText(mEvent.getDate() + ": "
	// + mEvent.getStartTime() + " "
	// + mEvent.getEndTime());
	//
	// TextView mCaseName = (TextView) dialog
	// .findViewById(R.id.et_show_case_name);
	// mCaseName.setText(getCaseName(mEvent.getCaseId()));
	//
	// TextView mDescription = (TextView) dialog
	// .findViewById(R.id.et_show_desccription);
	// mDescription.setText(mEvent.getDescription());
	//
	// int colorIndex = (int) (mEvent.getCaseId() % Constants.COLORS.length);
	// linearlayout.setBackgroundColor(getResources()
	// .getColor(Constants.COLORS[colorIndex]));
	//
	// dialog.show();
	// AppointmentActivity.popup = true;
	//
	// }
	// });
	// Button editButton = (Button) dialog
	// .findViewById(R.id.bt_show_edit);
	// // if button is clicked, close the custom dialog
	// editButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// Intent i = new Intent(getActivity(),
	// AddEventActivity.class);
	// i.putExtra(Constants.TYPE, Constants.EDIT);
	// i.putExtra(Constants.ID, mEvent.getId());
	// i.putExtra(Constants.SOURCE, Constants.WEEK);
	// i.putExtra(Constants.WORK, AppointmentActivity.source);
	// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
	// i.putExtra(Constants.CLIENT_ID,
	// AppointmentActivity.clientId);
	// }
	// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	// startActivity(i);
	// dialog.dismiss();
	// AppointmentActivity.popup = false;
	// }
	// });
	//
	// Button deleteButton = (Button) dialog
	// .findViewById(R.id.bt_show_delete);
	// // if button is clicked, close the custom dialog
	// deleteButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// delete(mEvent.getId());
	// refresh();
	// dialog.dismiss();
	// AppointmentActivity.popup = false;
	// }
	// });
	//
	// }
	// DateTime now = DateTime.now(TimeZone.getDefault());
	// int nowTime = (int) (((now.getHour() * 60) + now.getMinute())
	// * scale + 0.5f);
	// Button nowView = new Button(getActivity());
	// nowView.setBackgroundColor(getResources().getColor(R.color.Red));
	// LayoutParams viewparam = new LayoutParams(
	// LayoutParams.MATCH_PARENT, 1);
	// viewparam.setMargins(2, nowTime, 2, 0);
	// nowView.setLayoutParams(viewparam);
	//
	// linearlayout_full.addView(nowView);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG)
	// .show();
	// }
	//
	// }
	//
	// private DatabaseHelper getHelper() throws SQLException {
	// if (databaseHelper == null) {
	// databaseHelper = OpenHelperManager.getHelper(getActivity(),
	// DatabaseHelper.class);
	// }
	// return databaseHelper;
	// }

	public int minuteConverter(DateTime date) {
		int minute = date.getHour() * 60 + date.getMinute();
		return minute;
	}

	// public String getCaseName(long caseId) {
	// String caseName = "unable to load";
	// Case cases;
	// Dao<Case, Long> caseDao;
	// try {
	// caseDao = getHelper().getDao(Case.class);
	// cases = caseDao.queryForId(caseId);
	// caseName = cases.getName();
	// } catch (Exception e) {
	// Toast.makeText(getActivity(), "Error while loading case name",
	// Toast.LENGTH_LONG).show();
	// }
	//
	// return caseName;
	// }
	//
	// private void delete(long id) {
	// Dao<Event, Long> eventDao;
	// try {
	// eventDao = getHelper().getDao(Event.class);
	// eventDao.deleteById(id);
	// Event event = eventDao.queryForId(id);
	// DeleteBuilder<Event, Long> eventDelete = eventDao.deleteBuilder();
	// eventDelete.where().eq("id", id);
	// eventDelete.delete();
	// if (event.isBillable() == true) {
	// Intent intent = new Intent(getActivity(), BillReciever.class);
	// PendingIntent pendingIntent = PendingIntent.getBroadcast(
	// getActivity().getApplicationContext(), (int) id,
	// intent, 0);
	// getActivity().getBaseContext();
	// AlarmManager alarmManager = (AlarmManager) getActivity()
	// .getSystemService(Context.ALARM_SERVICE);
	// alarmManager.cancel(pendingIntent);
	// }
	// if (event.isReminder() == true) {
	// Intent intent = new Intent(getActivity(),
	// ReminderReciever.class);
	// PendingIntent pendingIntent = PendingIntent.getBroadcast(
	// getActivity().getApplicationContext(), (int) id,
	// intent, 0);
	// getActivity().getBaseContext();
	// AlarmManager alarmManager = (AlarmManager) getActivity()
	// .getSystemService(Context.ALARM_SERVICE);
	// alarmManager.cancel(pendingIntent);
	// }
	// Toast.makeText(getActivity(), "Event Deleted", Toast.LENGTH_LONG);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

	public void refresh() {
		FragmentManager manager = getActivity().getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		Fragment newFragment = this;
		this.onDestroy();
		ft.remove(this);
		ft.replace(this.getId(), newFragment);
		// container is the ViewGroup of current fragment
		ft.addToBackStack(null);
		ft.commit();
	}

}