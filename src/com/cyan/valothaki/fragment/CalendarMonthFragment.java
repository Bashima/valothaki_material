package com.cyan.valothaki.fragment;

import hirondelle.date4j.DateTime;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cyan.valothaki.R;
import com.cyan.valothaki.activity.user.AppointmentActivity;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

@SuppressLint("SimpleDateFormat")
public class CalendarMonthFragment extends CaldroidFragment {

	private CaldroidFragment caldroidFragment;
	public TextView tvSun;
	// private DatabaseHelper databaseHelper;
	// public List<Event> events;

	private OnDateSelectListener onDateSelectListener;

	@Override
	public void onResume() {
		super.onResume();
		caldroidFragment = new CaldroidFragment();
		Bundle args = new Bundle();
		Calendar cal = Calendar.getInstance();
		args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
		args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
		args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
		args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

		caldroidFragment.setArguments(args);

		android.support.v4.app.FragmentTransaction t = getActivity()
				.getSupportFragmentManager().beginTransaction();
		t.replace(R.id.calendar, caldroidFragment);
		t.commit();

		final CaldroidListener listener = new CaldroidListener() {

			@Override
			public void onSelectDate(Date date, View view) {
				DateTime today = DateTime.today(TimeZone.getDefault());
				Format formatter = new SimpleDateFormat("yyyy-MM-dd");
				String s = formatter.format(date);
				DateTime givenDate = new DateTime(s);
				if (givenDate.compareTo(today) > 0) {
					AppointmentActivity.viewT = 1;
					int diffInDays = today.numDaysFrom(givenDate);
					AppointmentActivity.viewI = diffInDays;
				} else if (givenDate.compareTo(today) < 0) {
					AppointmentActivity.viewT = -1;
					int diffInDays = today.numDaysFrom(givenDate);
					AppointmentActivity.viewI = diffInDays;
				}

				else {
					AppointmentActivity.viewT = 0;
					AppointmentActivity.viewI = 0;
				}

				onDateSelectListener.onDateSelected();
			}

		};

		caldroidFragment.setCaldroidListener(listener);

		// setEvent();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		onDateSelectListener = (OnDateSelectListener) activity;
	}

	// public void setEvent() {
	// try {
	// Dao<Event, Long> dao = getHelper().getDao(Event.class);
	// QueryBuilder<Event, Long> builder = dao.queryBuilder();
	// if(AppointmentActivity.source.equals(Constants.CLIENT)) {
	// Dao<Case, Long> caseDao = getHelper().getDao(Case.class);
	// QueryBuilder<Case, Long> caseBuilder = caseDao.queryBuilder();
	// caseBuilder.where().eq("clientId", AppointmentActivity.clientId);
	// Log.d("Client ID", String.valueOf(AppointmentActivity.clientId));
	// List<Case> cases = caseDao.query(caseBuilder.prepare());
	// List<Long> ids = new ArrayList<Long>();
	// for (Case caseItem : cases) {
	// ids.add(caseItem.getId());
	// }
	// builder.where().in("caseId", ids);
	// }
	// SimpleDateFormat formatter;
	// formatter = new SimpleDateFormat("yyyy-MM-dd");
	// events = dao.query(builder.prepare());
	// for (Event event : events) {
	//
	// Date date;
	//
	// date = (Date) formatter.parse(event.getDate());
	// if (caldroidFragment != null) {
	// caldroidFragment.setBackgroundResourceForDate(
	// com.caldroid.R.drawable.red_border_gray_bg, date);
	// caldroidFragment.setTextColorForDate(android.R.color.white,
	// date);
	// }
	// }
	// Date todate = new Date();
	//
	// caldroidFragment.setBackgroundResourceForDate(
	// com.caldroid.R.drawable.red_border, todate);
	// events = dao.query(builder.prepare());
	// for (Event event : events) {
	// Date date;
	// formatter = new SimpleDateFormat("yyyy-MM-dd");
	//
	// date = (Date) formatter.parse(event.getDate());
	// if (caldroidFragment != null) {
	// caldroidFragment.setBackgroundResourceForDate(
	// com.caldroid.R.drawable.red_border_gray_bg, date);
	// caldroidFragment.setTextColorForDate(android.R.color.black, date);
	// }
	// }
	// } catch (SQLException e) {
	// Toast.makeText(getActivity(), "Error while loading events.",
	// Toast.LENGTH_LONG).show();
	// e.printStackTrace();
	// } catch (ParseException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// private DatabaseHelper getHelper() throws SQLException {
	// if (databaseHelper == null) {
	// databaseHelper = OpenHelperManager.getHelper(getActivity(),
	// DatabaseHelper.class);
	// }
	// return databaseHelper;
	// }

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_calendar_month,
				container, false);
		setHasOptionsMenu(true);
		return view;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// switch (item.getItemId()) {
		// case R.id.action_add:
		// Intent i = new Intent(getActivity(), AddEventActivity.class);
		// i.putExtra(Constants.TYPE, Constants.ADD_NEW);
		// i.putExtra(Constants.SOURCE, Constants.MONTH);
		// i.putExtra(Constants.WORK, AppointmentActivity.source);
		// if (AppointmentActivity.source.equals(Constants.CLIENT)) {
		// i.putExtra(Constants.CLIENT_ID, AppointmentActivity.clientId);
		// }
		// startActivity(i);
		return true;
		// default:
		// return super.onOptionsItemSelected(item);
		// }
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.add_menu, menu);
	}

	public interface OnDateSelectListener {
		void onDateSelected();
	}
}