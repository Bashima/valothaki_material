package com.cyan.valothaki.item;

public class DoubleItem {

	public int id;
	public String text;
	public String number;

	public DoubleItem(int id, String text, String number) {
		super();
		this.id = id;
		this.text = text;
		this.number = number;
	}

}
