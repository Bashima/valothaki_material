package com.cyan.valothaki.item;

public class SingleItem {

	public int id;
	public String text;

	public SingleItem(int id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

}
